import { Component, ContentChild, NgModule  } from '@angular/core';
import { CommonModule } from '@angular/common'

import { ShowHideInput } from './show-hide-input'
export { ShowHideInput } from './show-hide-input'

@Component({
  selector: 'show-hide-container',
  template: `
      <ng-content></ng-content>
      <p><a (click)="toggleShow($event)">
      <i *ngIf="!show" class="fa fa-eye"></i>
      <i *ngIf="show" class="fa fa-eye-slash"></i>
      </a></p>
      `
})
export class ShowHideContainerComponent {
  private show: boolean = false;

  @ContentChild(ShowHideInput) input: ShowHideInput;

  toggleShow() {
    this.show = !this.show;

    if (this.show) {
      this.input.changeType("text");
    }
    else {
      this.input.changeType("password");
    }
  }
}

@NgModule({
  declarations: [
    ShowHideContainerComponent,
    ShowHideInput
  ],
  exports: [
    ShowHideContainerComponent,
    ShowHideInput
  ],
  imports: [
    CommonModule
  ]
})
export default class ShowHideContainer { }
