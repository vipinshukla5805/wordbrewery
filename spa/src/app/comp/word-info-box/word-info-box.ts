import {
  Component,
  Input,
  ChangeDetectionStrategy,
  NgModule,
  Output,
  EventEmitter
} from "@angular/core";
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';


import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription'
import { Observable } from 'rxjs/Observable'
import { Subject } from 'rxjs/Subject';

import { Word } from '../../model/word.class';
import { SpeakerService } from '../../model/speaker/speaker.class';
import { RequestStatus } from '../../model/requestStatus.class';

import { WordList } from '../../model/lists/wordList.class';
import { WordListService } from '../../model/lists/wordList.service';
import { TranslatorService } from '../../model/translator/translator.service';
import { AuthenticationService, IUserData, IUserStatisticData } from '../../model/user/authentication.service';
import { ListSettingService } from '../../model/settings/listSetting.service';
import { AudioService } from '../../model/core/audio'
import providerSentenceBlender, { SentenceBlenderService } from './model/wordinfobox.service'
import { ClientLists } from '../../model/settings/clientLists.class';

@Component({
  selector: 'word-info-box',
  templateUrl: '/app/comp/word-info-box/word-info-box.html'
})
export class WordInfoBoxComponent {
  @Input() mode: number = 1; // 1 = explore mode; 2 = course mode;
  @Input() emptyWordTitle: string;

  @Input() set word(word: Word) {
    this.clickedWord = word;
    this.cancelSubscription();

    if (!this.clickedWord) return;

    this._subs.push(
      this.listSetting.listSettings$
        .subscribe(data => {
          if (!data) return;
          this.settings = data.settings;
          this.lists = data.listSettings;
          this.initWordInfoBox();
          this.loadWordLists();
        }));
  }

  private _subs: Array<Subscription> = [];
  private settings: any;
  private clickedWord: Word;
  private relatedWordRequest: RequestStatus = new RequestStatus();
  private relatedWords: any = {
    token: null,
    glosses: [],
    pinyin: null
  };
  private requestStatus: RequestStatus = new RequestStatus();
  private wordListModalData: any;
  private wordLists: Array<WordList> = []
  private lists: ClientLists = new ClientLists();
  private listForm: FormGroup;
  private errorMsg: string = '';

  constructor(
    private translatorService: TranslatorService,
    private speaker: SpeakerService,
    private sentenceBlenderService: SentenceBlenderService,
    private wordListService: WordListService,
    private listSetting: ListSettingService,
    private audio: AudioService,
    private _fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.initNewListValidation();
  }

  ngOnDestroy() {
    this.cancelSubscription();
  }

  private initNewListValidation() {
    this.listForm = this._fb.group({
      'listName': [ '', Validators.required ],
      'listDescription': [ '' ]
    });
  }

  private cancelSubscription() {
    while (this._subs.length) this._subs.pop().unsubscribe();

    this.relatedWordRequest = {
      isError: false,
      isSuccess: false,
      isServerRequesting: false
    };

    this.relatedWords = {
      pinyin: null,
      token: null,
      glosses: []
    };
  }

  private initWordInfoBox() {
    this.speak(this.clickedWord);

    if (this.settings.currentLanguage.languageId === "English")
      this.showRelatedWords(this.clickedWord);
    else
      this.getTranslation();
  }

  private speak(word: Word): any {
    this.speaker.speak(word.text, this.settings.currentLanguage.languageId,
      this.settings.currentLevel);
  }

  private showRelatedWords(word: Word) {
    this.relatedWordRequest = {
      isError: false,
      isSuccess: false,
      isServerRequesting: true
    };

    this._subs.push(
      this.sentenceBlenderService.showRelatedWordsInfo(word.languageId, word.text, word.lemma, word.pos)
        .subscribe((res: any) => {
          if (res.status) {
            res = res.data;
            this.relatedWords = Object.assign({}, {
              token: res ? res.token : '',
              glosses: res && res.hasOwnProperty('cedictData') && res.ceditctData
                ? res.cedictData.glosses : res && res.hasOwnProperty('glosses') && res.glosses ? res.glosses : [],
              pinyin: res && res.hasOwnProperty('cedictData') && res.ceditctData
                ? res.cedictData.pinyin : null
            });
          }

          this.relatedWordRequest = {
            isError: false,
            isSuccess: true,
            isServerRequesting: false
          };
        }, () => {
          this.relatedWordRequest = {
            isError: true,
            isSuccess: false,
            isServerRequesting: false
          };
        }));
  }

  private getTranslation() {
    if (this.settings.currentLanguage.languageId == 'English') return;

    let text = this.clickedWord.lemma || this.clickedWord.text;
    if (this.clickedWord.hasOwnProperty('translation') && this.clickedWord.translation) {
      return;
    }

    this._subs.push(
      this.translatorService.getTranslation(text, this.settings.currentLanguage.languageId)
        .subscribe(data => {
          let translation = data ? data.replace(/"/g, '').toLowerCase() : '';
          this.clickedWord = Object.assign({}, this.clickedWord, {
            translation: translation
          })
        }));
  }

  private loadWordLists(): any {
    this._subs.push(
      this.wordListService.getlistsByLanguage(this.settings.currentLanguage.languageId)
        .subscribe(data => {
          this.wordLists = data.map((item) => {
            item.countryCode = this.getCountryCodeByLanguage(item.languageId)
            return item;
          });
        }, err => {
          this.wordLists = null;
          // do not show errors here
          // this.errors.show(err);
        }));
  }

  private getCountryCodeByLanguage(language) {
    if (this.lists.languages) {
      let country = this.lists.languages.filter((data: any) => {
        return data.hasOwnProperty('originalItem') && data.originalItem ?
          data.originalItem.languageId.indexOf(language) > -1
          : data.languageId.indexOf(language) > -1;
      })
      return country && country.length > 0 ? country[ 0 ].countryCode : '';
    }
  }

  private closeModal() {
    $('#sentenceModalWordList').hide();
  }

  private closeNewModal() {
    $('#createNewWordList').removeClass('is-visible');
  }

  private addWordToList(list: any): any {
    this.requestStatus = {
      isServerRequesting: true,
      isError: false,
      isSuccess: false
    };
    list.isSuccess = false;

    delete this.wordListModalData.translation;
    delete this.wordListModalData.relatedWords;

    this.audio.play('/assets/sound/ding.wav', 1);

    this._subs.push(
      this.wordListService.addEntry(this.wordListModalData, list.id)
        .subscribe(data => {
          this.requestStatus.isServerRequesting = false;
          this.requestStatus.isSuccess = true;
          list.isSuccess = true;
          this.requestStatus.isError = false;
          setTimeout(() => {
            this.closeModal()
            $('#createNewWordList').removeClass('is-visible');
          }, 1000)
        }, err => {
          this.requestStatus.isServerRequesting = false;
          this.requestStatus.isSuccess = false;
          this.requestStatus.isError = true;

          let error = err.json();
          this.errorMsg = error[ 'error' ];
        },
        () => {
          this.requestStatus.isServerRequesting = false;
          setTimeout(() => {
            this.requestStatus.isError = false;
            this.requestStatus.isSuccess = false;
          }, 1000);
        }
        ));
  }

  private openWordToList(data) {
    this.wordListModalData = data;

    this.requestStatus = {
      isServerRequesting: false,
      isSuccess: false,
      isError: false
    }
    this.errorMsg = '';
    $('#sentenceModalWordList').show()
  }

  private openCreateNewWordList() {
    this.closeModal();

    this.listForm.reset();

    this.requestStatus = {
      isServerRequesting: false,
      isSuccess: false,
      isError: false
    }
    this.errorMsg = '';
    $('#createNewWordList').addClass('is-visible')
  }

  private addNewList() {
    if (!this.listForm.dirty || !this.listForm.valid) return;

    this.requestStatus = {
      isServerRequesting: true,
      isError: false,
      isSuccess: false
    };

    this.wordListService.addlist(this.listForm.controls[ 'listName' ].value, this.listForm.controls[ 'listDescription' ].value,
      this.settings.currentLanguage.languageId)
      .subscribe(data => {
        this.loadWordLists();
        // list created, now adding the word
        this.addWordToList(data);
      }, (err) => {
        let error = err.json();
        this.errorMsg = error[ 'error' ];
        this.requestStatus = {
          isServerRequesting: false,
          isError: true,
          isSuccess: false
        };
      });
  }

}

@NgModule({
  declarations: [
    WordInfoBoxComponent
  ],
  exports: [
    WordInfoBoxComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  providers: [
    SentenceBlenderService
  ]
})
export class WordInfoBoxModule { }