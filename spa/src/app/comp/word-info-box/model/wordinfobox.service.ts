import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/interval'
import
{
	Injectable, NgModule
} from '@angular/core';
import
{
	Http, RequestMethod, Headers
} from '@angular/http';
import
{
	httpRequest
} from '../../../model/core/httpRequest';

@Injectable()
export class SentenceBlenderService {

  constructor(private http: Http){}

  getSentencePack(languageId: string, level: string, category: string) {
		let params = `languageId=${languageId}&level=${level}&category=${category}`;
    let req = {
      method: RequestMethod.Get,
      url: '/api/sentence',
      search: params
    };

    return httpRequest(this.http, req);
  }

	searchSentence(languageId: string, level:string, text: string) {
		let params = `languageId=${languageId}&level=${level}&text=${text}`;
    let req = {
      method: RequestMethod.Get,
      url: '/api/sentence',
      search: params
    };

    return httpRequest(this.http, req);
  }

	searchSentencePack(languageId: string, level:string, filter: Array<string>, text: string, lemma: string, pos: string) {
		let params = `languageId=${languageId}&level=${level}&text=${text}&lemma=${lemma}&pos=${pos}&filter=${encodeURIComponent(filter.join('|'))}`;
    let req = {
      method: RequestMethod.Get,
      url: '/api/sentence',
      search: params
    };

    return httpRequest(this.http, req);
  }

  report(sentenceData) {
    let req = {
      method: RequestMethod.Post,
      url: '/sentence/report.json',
      body: JSON.stringify(sentenceData)
    };

    return httpRequest(this.http, req);
  }

  showRelatedWordsInfo(languageId: string, text: string, lemma: string, pos: string) {
    let params = `languageId=${languageId}&text=${text}&lemma=${lemma}&pos=${pos}`;
    let req = {
      method: RequestMethod.Get,
      url: '/api/word/info',
      search: params
    };

    return httpRequest(this.http, req);

  }
}

@NgModule({
  providers: [
    SentenceBlenderService
  ]
})
export default class providerSentenceBlender {}
