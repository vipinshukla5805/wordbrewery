declare const FB: any;
declare const window: any;

export interface IUserResponse {
  status: boolean;
  msg: string;
  data: IFBUser;
}

export interface IFBUser {
  accessToken: string;
}

export class FBService {

  constructor(private appId: string) {
    if (!window.fbAsyncInit) {
      window.fbAsyncInit = () => {
        FB.init({
          appId: this.appId,
          cookie  : true,
          version: 'v2.6'
        });
      };
    }
    /* init fb sdk */
    let js: any,
      id: string = 'facebook-jssdk',
      ref: any = document.getElementsByTagName('script')[0];

    if (document.getElementById(id)) {
      return;
    }

    js = document.createElement('script');
    js.id = id;
    js.async = true;
    js.src = '//connect.facebook.net/en_US/sdk.js';

    ref.parentNode.insertBefore(js, ref);
  }

  public fbLogin() {
    return new Promise((resolve: any, reject: any) => {
        FB.login((response: any) => {
          if (response.authResponse) {
            resolve({
              data: response.authResponse.accessToken,
              msg: 'User logged in successfully.',
              status: true
            });
          } else {
            reject({
              data: null,
              msg: 'User cancelled login or did not fully authorize.',
              status: false
            });
          }
        }, { scope: 'email' });
    });
  }
}