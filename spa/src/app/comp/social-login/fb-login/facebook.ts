import { Component, Input, Output, EventEmitter, NgModule  } from '@angular/core'
import { FBService } from './facebook.service'
import { Router } from '@angular/router'

const FACEBOOK_APP_ID = '107695666327886';
// const FACEBOOK_APP_ID = '827888180573972';

@Component({
  selector: 'wb-facebook',
  template: `
  <div class="col-md-6 col-xs-6">
    <button type="button"  class="btn btn-block" (click)="login()">
    <i class="fa fa-facebook"></i> Facebook</button>
  </div>  `
})

export class FacebookComponent {
  private fb: FBService;

  @Output() onLogin: EventEmitter<any> = new EventEmitter<any>();

  constructor(private router: Router) {
    this.fb = new FBService(FACEBOOK_APP_ID);
  }

  login() {
    if (true || navigator.userAgent.match('CriOS')) {
      let redirect_uri = `${document.location.origin}/fb-login-success.html`
      window.open('https://www.facebook.com/dialog/oauth?client_id=' + FACEBOOK_APP_ID + '&redirect_uri=' + redirect_uri + '&scope=email,public_profile', '_self', null);
    }
    // else {
    //   this.fb.fbLogin().then((res: any) => {
    //     if (res.status) {
    //       this.onLogin.next(res);
    //     }
    //   }, (errResp) => { });
    // }
  }
}

@NgModule({
  declarations: [
    FacebookComponent
  ]
})
export default class FBComponent { }
