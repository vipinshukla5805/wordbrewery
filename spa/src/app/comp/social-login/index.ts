import { NgModule } from '@angular/core';

import { GoogleComponent } from './google-login/google';
import { FacebookComponent } from './fb-login/facebook'

@NgModule({
    declarations: [
        FacebookComponent,
        GoogleComponent
    ],
    exports: [
        FacebookComponent,
        GoogleComponent
    ]
})
export default class SocialLoginModule { }