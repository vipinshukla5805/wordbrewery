import { Component, Input, Output, EventEmitter, NgZone } from '@angular/core'

declare const gapi: any;

@Component({
    selector: 'wb-google',
    template: `
          <div class="col-md-6 col-xs-6">
            <button class="btn btn-block" (click)="onLogin();" type="button">
            <i class="fa fa-google-plus"></i> Google</button>
          </div>`
})

export class GoogleComponent {
    @Output('onLogin') onLoginSuccess: EventEmitter<any> = new EventEmitter<any>();

    constructor(private _zone: NgZone) { }

    onLogin() {
        if (true || navigator.userAgent.match('CriOS')) {
           let redirect_uri = `${document.location.origin}/google-login-success.html`
           let url =  `https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.email&client_id=581130924510-okoefeloopqgejj2gfack2a4mi60gd76.apps.googleusercontent.com&redirect_uri=${redirect_uri}&response_type=id_token`;
           window.open(url, '_self', null);
        }
        // else {
        //     let myParams = {
        //         'clientid': '581130924510-okoefeloopqgejj2gfack2a4mi60gd76', 
        //         'cookiepolicy': 'single_host_origin',
        //         'callback': this.onGoogleLoginSuccess,
        //         'approvalprompt': 'force',
        //         'scope': 'https://www.googleapis.com/auth/plus.login'
        //     };
        //     gapi.auth.signIn(myParams);
        // }
    }

    onGoogleLoginSuccess = (result) => {
        this._zone.run(() => {
            this.onLoginSuccess.next(result.id_token)
        });
    }
}