import { Component, NgModule, Input, Output, EventEmitter } from '@angular/core';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/takeUntil';

import { Popovers } from '../../model/popovers/popovers';
import { RequestStatus } from '../../model/requestStatus.class';
import { TranslatorService } from '../../model/translator/translator.service';
import { SpeakerService } from '../../model/speaker/speaker.class';
import { SentenceListService } from '../../model/lists/sentenceList.service';
import { SentenceList } from '../../model/lists/sentenceList.class';
import { Word } from '../../model/word.class';
import { ClientLists } from '../../model/settings/clientLists.class';
import { ListSettingService } from '../../model/settings/listSetting.service';
import { AudioService } from '../../model/core/audio'

@Component({
  selector: 'sentence-info-box',
  templateUrl: '/app/comp/sentence-info-box/sentence-info-box.html'
})
export class SentenceInfoBoxComponent {
  @Input() isRTL: boolean = false;
  @Input() mode: number = 1;
  @Input() isLoading: boolean = false;
  @Input() set data(value: any) {
    this.sentence = value;
    if (this.sentence) {
      this._subs.push(
        this.listSetting.listSettings$
          .subscribe(data => {
            if (!data) return;

            this.settings = data.settings;
            this.lists = data.listSettings;
            this.speak(this.sentence.text);
            this.loadSentenceLists();
          }));
    }
  }

  @Output() onReport: EventEmitter<any> = new EventEmitter<any>();
  @Output() onWordClick: EventEmitter<any> = new EventEmitter<any>();

  private _subs: Array<Subscription> = [];
  private settings: any;
  private sentence: any;
  private translationSubscriber: Subscription;
  private timer: any;
  private isTranslating: boolean = false;
  private sentenceLists: Array<SentenceList> = [];
  private requestStatus: RequestStatus = new RequestStatus();
  private lists: ClientLists = new ClientLists();
  private listForm: FormGroup;
  private sentenceListModalData: any;
  private wordListModalData: any;
  private errorMsg: string = '';

  constructor(
    private popovers: Popovers,
    private translatorService: TranslatorService,
    private speaker: SpeakerService,
    private sentenceListService: SentenceListService,
    private listSetting: ListSettingService,
    private _fb: FormBuilder,
    private audio: AudioService
  ) { }

  ngOnInit() {
    this.initNewListValidation();
  }

  ngOnDestroy() {
    this.cancelSubscription();
  }

  private onMouseOverWord(event: any, word: Word): any {
    //this.popovers.hide();
    if (word.isPunctuation || this.isTranslating) return;
    this.timer = setTimeout(() => {
      this.getWordTranslation(word, event.target.parentElement);
    }, 100)
  }

  private onMouseOutWord(event: any, word: Word): any {
    clearTimeout(this.timer);
    if (this.translationSubscriber)
      this.translationSubscriber.unsubscribe()
    this.isTranslating = false;
  }

  private onMouseLeaveWord(event: any, word: Word): any {
    this.popovers.hide();
    this.isTranslating = false;
  }

  private initNewListValidation() {
    this.listForm = this._fb.group({
      'listName': [ '', Validators.required ],
      'listDescription': [ '' ]
    });
  }

  private onWordClicked(event: any, word: Word, sentence: any): any {
    if (word.isPunctuation) return;

    this.cancelSubscription();

    if (this.settings.currentLanguage.isWebSpeechAPISupported) {
      this.speak(word.text);
    }

    // emit the selected word to load more sentences
    this.onWordClick.next({ word: word, sentence: sentence });
  }

  private speak(text: string): any {
    this.speaker.speak(text, this.settings.currentLanguage.languageId,
      this.settings.currentLevel);
  }

  private getWordTranslation(word: Word, target: any = null) {
    if (this.isTranslating || this.settings.currentLanguage.languageId == 'English') return;

    let text = word.lemma || word.text;
    if (word.hasOwnProperty('translation') && word.translation) {
      this.showTranslationPopover(target, word.translation, word);
      return;
    }

    this.isTranslating = true;

    this.translationSubscriber = this.translatorService.getTranslation(text, this.settings.currentLanguage.languageId)
      .subscribe(data => {
        let translation = data ? data.replace(/"/g, '').toLowerCase() : '';
        word.translation = translation;
        this.showTranslationPopover(target, translation, word);
      },
      null,
      () => {
        this.isTranslating = false;
      });
  }

  private showTranslationPopover(target, data, word) {
    this.popovers.hide();
    let html = '';
    if (data) {
      html = `<span class="text-active">${data ? data.replace(/"/g, '').toLowerCase() : ''}</span>`;
    }
    else {
      html = `
        <p><a target="_blank"
        href="https://www.google.com/search?q=${encodeURIComponent(word.text)}&ie=UTF-8"
        onclick="event.stopImmediatePropagation()"
        >Google</a></p>

        <p><a target="_blank"
        onclick="event.stopImmediatePropagation()"
        href="http://${this.settings.currentLanguage.languageCode}.wiktionary.org/wiki/${encodeURIComponent(word.text)}"
        >Wiktionary</a></p>
        `;
    }
    target.setAttribute('data-original-title', '');
    target.setAttribute('data-html', 'true');
    target.setAttribute('data-placement', 'top');
    target.setAttribute('data-content', html);
    setTimeout(() => $(target).popover('show'), 250);
  }

  private cancelSubscription() {
    while (this._subs.length) {
      this._subs.pop().unsubscribe()
    }
  }

  private translateSenetence(sentence, target) {
    this.popovers.hide();

    if (sentence && sentence.hasOwnProperty('translations') && sentence.translations.length > 0) {
      this.showSentenceTranslationPopup(sentence.translations, target);
    }
    else {
      this.translatorService.getSentenceTranslations(sentence.text, this.settings.currentLanguage.languageId)
        .subscribe(res => {
          sentence.translations = res;
          this.showSentenceTranslationPopup(res, target);
        })
    }
  }

  private showSentenceTranslationPopup(data, target) {
    let html = '';
    if (data.length <= 0) {
      html = '<span class="text-active">No translation is available</span>';
    }
    else {
      let adminTranslation = this.adminTranslationAvailable(data);
      if (adminTranslation) {
        html = `
          <h5 class="text-active">${adminTranslation.translation}
          </h5>`;
      }
      else {
        html = `
      <h5 class="text-active">${data[ 0 ].translation}
         <small><cite title = "${data[ 0 ].source}">${data[ 0 ].source}</cite></small>
      </h5>
      <h5 class="text-active">${data[ 1 ].translation}
         <small><cite title = "${data[ 1 ].source}">${data[ 1 ].source}</cite></small>
      </h5>`;
      }
    }
    $('span:eq(0)', target).attr({
      'data-placement': 'top',
      'data-html': 'true',
      'title': '',
      'data-content': html
    });
    setTimeout(() => {
      $('span:eq(0)', target).popover('show');
    }, 100)
  }

  private adminTranslationAvailable(data) {
    let res = data.filter((item) => item.source === 'Admin')
    return res != null ? res[ 0 ] : false;
  }

  // added code for handling list
  private loadSentenceLists() {
    this.requestStatus = {
      isServerRequesting: true,
      isSuccess: false,
      isError: false
    }

    this._subs.push(
      this.sentenceListService.getlistsByLanguage(this.settings.currentLanguage.languageId)
        .subscribe(data => {
          this.sentenceLists = data.map((item) => {
            item.countryCode = this.getCountryCodeByLanguage(item.languageId)
            return item;
          });
          this.requestStatus.isServerRequesting = false;
          this.requestStatus.isError = false;
        }, err => {
          this.sentenceLists = null;

          this.requestStatus.isServerRequesting = false;
          this.requestStatus.isError = true;

          let error = err.json();
          this.errorMsg = error[ 'error' ];
        }
        ));
  }

  private getCountryCodeByLanguage(language) {
    if (this.lists.languages) {
      let country = this.lists.languages.filter((data: any) => {
        return data.hasOwnProperty('originalItem') && data.originalItem ?
          data.originalItem.languageId.indexOf(language) > -1
          : data.languageId.indexOf(language) > -1;
      })
      return country && country.length > 0 ? country[ 0 ].countryCode : '';
    }
  }

  private openSentenceToList(data: any, event: any) {
    this.requestStatus = {
      isServerRequesting: false,
      isSuccess: false,
      isError: false
    }
    this.errorMsg = '';

    this.sentenceListModalData = data;
    $('#senetenceModalList').show()
  }

  private closeModal() {
    $('#senetenceModalList').hide();
  }

  private closeNewModal() {
    $('#createNewSentenceList').removeClass('is-visible');
  }

  private openCreateNewSentenceList() {
    this.closeModal();

    this.listForm.reset();
    this.requestStatus = {
      isServerRequesting: false,
      isSuccess: false,
      isError: false
    }
    this.errorMsg = '';

    $('#createNewSentenceList').addClass('is-visible')
  }

  private addNewList() {
    if (this.listForm.dirty && !this.listForm.valid) return;

    this.requestStatus = {
      isServerRequesting: true,
      isError: false,
      isSuccess: false
    };

    this.sentenceListService.addlist(this.listForm.controls[ 'listName' ].value, this.listForm.controls[ 'listDescription' ].value,
      this.settings.currentLanguage.languageId)
      .subscribe(data => {
        this.loadSentenceLists();
        // list created, now adding the word
        this.addSentenceToList(data);
      });
  }

  private addSentenceToList(list: any): any {
    this.requestStatus = {
      isServerRequesting: true,
      isError: false,
      isSuccess: false
    };
    list.isSuccess = false;

    this.audio.play('/assets/sound/ding.wav', 1);

    this._subs.push(
      this.sentenceListService.addEntry(this.sentenceListModalData, list.id)
        .subscribe(data => {
          this.requestStatus.isServerRequesting = false
          this.requestStatus.isSuccess = true;
          this.requestStatus.isError = false;
          list.isSuccess = true;
          setTimeout(() => {
            this.closeModal();
            $('#createNewSentenceList').removeClass('is-visible');
          }, 1000)
        }, err => {
          this.requestStatus.isServerRequesting = false;
          list.isSuccess = false;
          let error = err.json();
          this.errorMsg = error[ 'error' ];
        }, () => {
          this.requestStatus.isServerRequesting = false
          setTimeout(() => {
            this.requestStatus.isError = false;
            this.requestStatus.isSuccess = false;
          }, 1000);
        }));
  }
}

@NgModule({
  declarations: [ SentenceInfoBoxComponent ],
  exports: [ SentenceInfoBoxComponent ],
  imports: [ CommonModule, ReactiveFormsModule ]
})
export class SentenceInfoBoxModule { }