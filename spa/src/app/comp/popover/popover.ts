
import { PopoverDirective } from './components/popover.directive';
import { PopoverContent } from './components/popover-container.component.ts';

export const POPOVER_DIRECTIVES =  [ PopoverContent, PopoverDirective ];