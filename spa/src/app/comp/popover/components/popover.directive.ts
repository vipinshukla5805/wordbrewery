import {
    Directive, HostListener, ComponentRef, NgModule,
    ViewContainerRef, ComponentFactoryResolver, ComponentFactory,
    Input, ReflectiveInjector } from "@angular/core";

import { PopoverContent } from "./popover-container.component";
import { PopoverOptions } from "./popover-options.class";

@Directive({ selector: "[popover]" })
export class PopoverDirective {
    private viewContainerRef: ViewContainerRef;
    private resolver: ComponentFactoryResolver;

    private visible: boolean = false;
    private popover: ComponentRef<any>;

    @Input("popover") content: string | PopoverContent;
    @Input() popoverDisabled: boolean;
    @Input() popoverAnimation: boolean = true;
    @Input() popoverPlacement: "top" | "bottom" | "left" | "right" = "bottom";
    @Input() popoverTitle: string;
    @Input() popoverOnHover: boolean = false;
    @Input() popoverCloseOnClickOutside: boolean = false;
    @Input() popoverCloseOnMouseOutside: boolean = false;

    public constructor(viewContainerRef: ViewContainerRef, resolver: ComponentFactoryResolver) {
        this.viewContainerRef = viewContainerRef;
        this.resolver = resolver;
    }

    @HostListener("click")
    showOrHideOnClick(): void {
        if (this.popoverOnHover) return;
        if (this.popoverDisabled) return;

        if (!this.visible) {
            this.show();
        } else {
            this.hide();
        }
    }

    // params event, target
    @HostListener('focusin', ['$event', '$target'])
    @HostListener('mouseenter', ['$event', '$target'])
    showOnHover(): void {
        if (!this.popoverOnHover || this.popoverDisabled || this.visible) return;

        this.show();
    }

    // params event, target
    @HostListener('focusout', ['$event', '$target'])
    @HostListener('mouseleave', ['$event', '$target'])
    hideOnHover(): void {
        if (this.popoverCloseOnMouseOutside
            || !this.popoverOnHover
            || this.popoverDisabled
            || !this.visible) return; // don't do anything since not we control this

        this.hide();
    }

    private show() {
        this.visible = true;
        this.getInstance().then((popoverInstance: PopoverContent) => {
            Object.assign(popoverInstance, this.setOptions())

            popoverInstance.onCloseFromOutside.subscribe(() => this.hide());
            popoverInstance.show();
        });
    }

    private getInstance() {
        return new Promise((resolve, reject) => {
            if (this.content && typeof this.content !== "string") {
                resolve(this.content as PopoverContent)
            }
            else {
                let modalFactory = this.resolver.resolveComponentFactory(<any>this.content);
                this.popover = this.viewContainerRef.createComponent(modalFactory);
                resolve(this.popover.instance as PopoverContent)
            }
        });
    }

    private hide() {
        this.visible = false;
        if (this.popover)
            this.popover.destroy();

        if (this.content instanceof PopoverContent)
            (this.content as PopoverContent).hide();
    }

    private setOptions() {
        let options = new PopoverOptions({
            content: this.content,
            title: this.popoverTitle,
            placement: this.popoverPlacement,
            animation: this.popoverAnimation,
            closeOnClickOutside: this.popoverCloseOnClickOutside,
            closeOnMouseOutside: this.popoverCloseOnMouseOutside,
            hostElement: this.viewContainerRef.element.nativeElement
        });

        return options;
    }
}