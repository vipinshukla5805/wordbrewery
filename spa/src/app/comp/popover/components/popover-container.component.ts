import {
    Component, 
    Input, 
    AfterViewInit, 
    ElementRef, 
    ChangeDetectorRef, 
    OnDestroy, 
    ViewChild, 
    EventEmitter,
    NgModule
} from "@angular/core";

import { positionService } from '../position'

@Component({
    selector: "popover-content",
    template: `
<div #popoverDiv class="popover {{ placement }}"
     [style.top]="top + 'px'"
     [style.left]="left + 'px'"
     [class.in]="isIn"
     [class.fade]="animation"
     style="display: block"
     role="tooltip">
    <div [hidden]="!closeOnMouseOutside" class="virtual-area"></div>
    <div class="arrow"></div> 
    <h3 class="popover-title" [hidden]="!title">{{ title }}</h3>
    <div class="popover-content">
        <ng-content></ng-content>
    </div> 
</div>
`,
})
export class PopoverContent implements AfterViewInit, OnDestroy {
    @Input() hostElement: HTMLElement;
    @Input() content: string;
    @Input() placement: "top"|"bottom"|"left"|"right" = "bottom";
    @Input() title: string;
    @Input() animation: boolean = true;
    @Input() closeOnClickOutside: boolean = false;
    @Input() closeOnMouseOutside: boolean = false;

    @ViewChild("popoverDiv")
    popoverDiv: ElementRef;

    onCloseFromOutside = new EventEmitter();

    top: number = -1000;
    left: number = -1000;
    isIn: boolean = false;
    displayType: string = "none";

    /**
     * Closes dropdown if user clicks outside of this directive.
     */
    onDocumentMouseDown = (event: any) => {
        const element = this.element.nativeElement;
        if (!element || !this.hostElement) return;
        if (element.contains(event.target) || this.hostElement.contains(event.target)) return;
        this.hide();
        this.onCloseFromOutside.emit(undefined);
    };

    constructor(private element: ElementRef,
                private cdr: ChangeDetectorRef) {          
    }

    ngAfterViewInit(): void {
        if (this.closeOnClickOutside)
            document.addEventListener("mousedown", this.onDocumentMouseDown);
        if (this.closeOnMouseOutside)
            document.addEventListener("mouseover", this.onDocumentMouseDown);

        this.show();
        this.cdr.detectChanges();
    }

    ngOnDestroy() {
        if (this.closeOnClickOutside)
            document.removeEventListener("mousedown", this.onDocumentMouseDown);
        if (this.closeOnMouseOutside)
            document.removeEventListener("mouseover", this.onDocumentMouseDown);
    }
    
    show(): void {
        if (!this.hostElement)
            return;

        const p = positionService.positionElements(this.hostElement, this.popoverDiv.nativeElement, this.placement);
        this.displayType = "block";
        this.top = p.top;
        this.left = p.left;
        this.isIn = true;
    }

    hide(): void {
        this.top = -1000;
        this.left = -1000;
        this.isIn = true;
    }

}