import {Injectable} from '@angular/core';

@Injectable()
export class PopoverOptions {
  public placement:string;
  public title:string;
  public animation:boolean;
  public closeOnClickOutside:boolean;
  public content:string;
  public closeOnMouseOutside: boolean;

  public constructor(options:Object) {
    Object.assign(this, options);
  }
}