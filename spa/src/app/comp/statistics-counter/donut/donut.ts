import { Component, Input, ElementRef, ViewChild } from '@angular/core';

@Component({
    selector: 'word-mastered',
    template: `
        
        <div class="tile text-center chartist-container ct-chart">
		 		<label><span>{{wordMastered}}</span>/{{wordTotal}}</label>
		 		<h1>{{title}}-level words</h1>
		</div>
    `

})
export class WordMasterdComponent {
    @Input() title: string;
    @Input() wordMastered: number;
    @Input() wordTotal: number;

    private chartOptions: any;
}