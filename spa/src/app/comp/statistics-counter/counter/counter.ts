import { Component, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'word-counter',
  template: `<div class="tile text-center">
		 	<h1 *ngIf="title">{{title}}</h1>
             <img *ngFor="let count of formattedCounters" src="/images/counter/{{count}}.jpg" />
		 	<h1>
                <img src="{{imageUrls}}" /> <span style="text-transform: capitialize;">{{language}} {{counterType}}</span>
            </h1>
		</div><!--tile-->`
})
export class WordCounterComponent {
  @Input() title: string;
  @Input() language: string;
  @Input() wordMastered: string;
  @Input() counterType: string;

  private get formattedCounters() {
    let pad = '0000';
    if (this.wordMastered) {
      let str = this.wordMastered.toString();
      let ans = pad.substring(0, pad.length - str.length) + str;
      return ans.split('')
    }

    return pad.split('');
  }

  // TODO : Need to optimize
  private get imageUrls() {
    if (this.language) {
      return "images/flag/" + this.language + ".png";
    } else {
      return "images/flag/English.png";
    }
  }


}