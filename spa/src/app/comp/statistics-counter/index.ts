import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

export * from './counter/counter';
export * from './donut/donut';

import { WordCounterComponent } from './counter/counter';
import { WordMasterdComponent }  from './donut/donut';

export const STATISTICS_DIRECTIVES = [
    WordCounterComponent,
    WordMasterdComponent
]

@NgModule({
    declarations : [
        STATISTICS_DIRECTIVES
    ],
    exports: [
        STATISTICS_DIRECTIVES
    ],
    imports: [
        CommonModule
    ]
})
export default class StatisticsModule {}