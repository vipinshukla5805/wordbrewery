import { Component, Input, Output, EventEmitter, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

export interface IDropdownOption {
  text: string
  value: string
  selected: boolean
  class?: string
  originalItem?: any
}

@Component({
  selector: 'wb-dropdown',
  template: `<div class="btn-group">
    <a *ngIf="selectedItem" class="btn btn-primary dropdown-toggle btn-block"
    data-toggle="dropdown" href="#" aria-expanded="false">
    <span class="{{selectedItem.class}}"></span> {{selectedItem.text}}
    <span class="caret"></span></a>
    <a *ngIf="!selectedItem" class="btn btn-primary dropdown-toggle btn-block"
    data-toggle="dropdown" href="#" aria-expanded="false">
    Select <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li *ngFor="let item of items" (click)="onSelect(item)">
        <a href="javascript:void(0);">
          <span class="{{item.class}}"></span> {{item.text}}
          <i [ngClass]="{'fa fa-check text-success': item.selected}"></i>
        </a>
      </li>
    </ul>
  </div>`
})

export class DropdownComponent {
  private selectedItem: IDropdownOption;
  private items: Array<IDropdownOption> = [];

  @Input() initSelect: string
  @Input('list') ddlItems: Array<IDropdownOption> = [];
  @Output('onSelect') onSelection$: EventEmitter<IDropdownOption> =
  new EventEmitter<IDropdownOption>();

  ngOnInit() {
    let result = this.ddlItems.filter(data => data.selected);
    this.items = this.ddlItems.filter(data => !data.selected);
    if (result) {
      this.selectedItem = result[0];
    }
  }

  onSelect(data) {
    this.ddlItems.map((data) => data.selected = false);
    this.items = this.ddlItems.filter(item => item.value != data.value);

    data.selected = true;
    this.selectedItem = data;

    let result = this.selectedItem.hasOwnProperty('originalItem') ? 
                 this.selectedItem.originalItem : this.selectedItem;
    this.onSelection$.next(result)
  }
}

@NgModule({
  declarations: [
    DropdownComponent
  ],
  exports: [
    DropdownComponent
  ],
  imports: [
    CommonModule
  ]
})
export default class DropdownModule {}
