import { Component, Input, ChangeDetectionStrategy, NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { Subscription } from 'rxjs/Subscription'

import { AuthenticationService } from '../../model/user/authentication.service'

@Component({
    selector: 'wb-footer-new',
    templateUrl: '/app/comp/footer/common-footer.html'
})

export class CommonFooterComponent {
    private isAdmin: boolean = false;
    private _subs: Subscription[] = [];

    @Input() appVersion: string = '1.0.1';

    constructor(private userSvc: AuthenticationService) {
        this._subs.push(
            this.userSvc.userData$.subscribe((res: any) => {
                if (res && res.status) {
                    this.isAdmin = res.data.role == 'admin';
                }
                else {
                    this.isAdmin = false;
                }
            }));
    }

    ngOnDestroy() {
        while (this._subs.length) this._subs.pop().unsubscribe();
    }
}

@NgModule({
    declarations: [
        CommonFooterComponent
    ],
    exports: [
        CommonFooterComponent
    ],
    imports: [
        RouterModule,
        CommonModule
    ]
})
export default class CommonFooterModule { }
