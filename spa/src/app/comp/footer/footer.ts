import { Component, Input, ChangeDetectionStrategy, NgModule } from "@angular/core";

import { Subscription } from 'rxjs/Subscription'

import { AuthenticationService } from '../../model/user/authentication.service'

import { AppSettings } from '../../model/core/appSetting'

@Component({
    selector: 'wb-footer',
    templateUrl: '/app/comp/footer/footer.html'
})

export class FooterComponent {
    private isAdmin: boolean = false;
    private _subs: Subscription[] = [];

    appVersion: string;

    constructor(private userSvc: AuthenticationService) {
        this._subs.push(
            this.userSvc.userData$.subscribe((res: any) => {
                if (res && res.status) {
                    this.isAdmin = res.data.isAdmin == 'admin';
                }
                else {
                    this.isAdmin = false;
                }
            }));
    }

    ngOnInit() {
        this.appVersion = AppSettings.WB_VERSION;
    }

    ngOnDestroy() {
        while (this._subs.length) this._subs.pop().unsubscribe();
    }
}
