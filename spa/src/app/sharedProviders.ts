import { NgModule, NgModuleFactoryLoader } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LocationStrategy, HashLocationStrategy, Location, CommonModule } from '@angular/common';
import { HttpModule, BrowserXhr, JsonpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, platformBrowser } from '@angular/platform-browser';

import { CORSBrowserXHr } from './model/core/httpRequest';
import { AsyncNgModuleLoader } from './model/core/asyncLoader';

import { SettingsService } from './model/settings/settings.service';
import { Errors } from './model/utils/errors.util';
import { AudioService } from './model/core/audio'
import { SpeakerService } from './model/speaker/speaker.class'
import { TranslatorService } from './model/translator/translator.service'
import { DefinitionsService } from './model/definitions/definitions.service'
import { ClientListsService } from './model/settings/clientLists.service'
import { ReportService } from './model/report/report.service'
import { Popovers } from './model/popovers/popovers'
import { AuthenticationService, UserService } from './model/user/authentication.service'
import { LIST_SETTINGS_PROVIDERS } from './model/settings/listSetting.service'

export const PROVIDERS = [
    AudioService,
    AuthenticationService,
    UserService,
    ClientListsService,
    DefinitionsService,
    LIST_SETTINGS_PROVIDERS,
    Popovers,
    ReportService,
    SpeakerService,
    SettingsService,
    TranslatorService,
    { provide: BrowserXhr, useClass: CORSBrowserXHr }
];

export const COMMON_MODULES = [
    BrowserModule,
    CommonModule,
    HttpModule,
    JsonpModule,
    FormsModule,
    ReactiveFormsModule
];

@NgModule({
    providers: [
        ...PROVIDERS
    ]
})
export default class SharedPorviderModule { }
