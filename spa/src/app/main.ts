import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import WordBreweryModule from './appModule'

platformBrowserDynamic().bootstrapModule(WordBreweryModule)
