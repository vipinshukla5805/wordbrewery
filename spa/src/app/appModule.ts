import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LocationStrategy, HashLocationStrategy, Location, CommonModule } from '@angular/common';

import { COMPONENTS, LayoutComponent } from './routes/layout/layout';

import { routing, appRoutingProviders, FEATURE_MODULES } from './routing';

import { WordbreweryComponent } from './routes/root/root';

import SharedPorviderModule, { PROVIDERS, COMMON_MODULES } from './sharedProviders';

import { AuthenticationService } from './model/user/authentication.service';
import DropdownModule from './comp/dropdown/dropdown';

@NgModule({
  bootstrap: [
    WordbreweryComponent
  ],
  declarations: [
    WordbreweryComponent,
    LayoutComponent,
    COMPONENTS
  ],
  imports: [
    COMMON_MODULES,
    SharedPorviderModule,
    FEATURE_MODULES,
    routing
  ],
  providers: [
    appRoutingProviders,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ]
})
export default class WordBreweryModule { }
