import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { load } from './model/core/asyncLoader';
import { CanActivateViaAuthGuard } from './model/core/authGaurd';

import FrontPageModule, { FrontPageComponent } from './routes/front/front';
import List, { ListComponent } from './routes/list/list';
import ListDetails, { ListDetailsComponent } from './routes/list/details/list-details';
import SentenceBlenderModule, { SentenceBlenderComponent } from './routes/explore/sentenceBlender';
import About, { AboutComponent } from './routes/about/about';
import TermsConditionsModule, { TermsConditionsComponent } from './routes/terms-conditions/terms-conditions';
import FourOhFour, { FourOhFourComponent } from './routes/fourohfour/fourohfour';

import * as account from './routes/account';
import * as course from './routes/courses'
import * as user from './routes/user'

export const FEATURE_MODULES = [
  FourOhFour,
  FrontPageModule,
  SentenceBlenderModule,
  List,
  ListDetails,
  About,
  TermsConditionsModule,
  account.AccountModule,
  account.LoginModule,
  account.RegisterModule,
  account.ForgotPasswordModule,
  account.AccountCreatedModule,
  account.ActivateModule,
  course.CourseModule,
  course.CourseDashboardModule,
  course.CourseDetailModule,
  course.CourseHomeModule,
  course.CourseStudyModule,
  course.CourseQuizModule,
  // user routers
  user.UserModule,
  user.AccountProfileModule,
  user.SettingsProfileModule,
  user.GeneralProfileModule,
  user.ChangePasswordModule,
  user.StatisticProfileModule,
  user.PlansProfileModule
];

export const appRoutes: Routes = [
  {
    path: '',
    component: FrontPageComponent
  },
  {
    path: 'explore',
    canActivate: [ CanActivateViaAuthGuard ],
    component: SentenceBlenderComponent
  },
  {
    path: 'list',
    canActivate: [ CanActivateViaAuthGuard ],
    component: ListComponent
  },
  {
    path: 'list/:id/:type',
    canActivate: [ CanActivateViaAuthGuard ],
    component: ListDetailsComponent
  },
  {
    path: 'account',
    canActivate: [ CanActivateViaAuthGuard ],
    component: account.AccountComponent,
    children: [
      {
        path: 'login',
        component: account.LoginComponent
      },
      {
        path: 'register',
        component: account.RegisterComponent
      },
      {
        path: 'register/:email',
        component: account.AccountCreatedComponent
      },
      {
        path: 'activate/:email/:code',
        component: account.ActivateComponent
      },
      {
        path: 'forgot',
        component: account.ForgotPasswordComponent
      }
    ]
  },
  {
    path: 'terms',
    component: TermsConditionsComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'fourohfour',
    component: FourOhFourComponent
  },
  {
    path: 'course',
    canActivate: [ CanActivateViaAuthGuard ],
    component: course.CourseComponent,
    children: [
      {
        path: 'dashboard/:code',
        component: course.CourseDashboardComponent
      },
      {
        path: 'details/:code',
        component: course.CourseDetailComponent
      },
      {
        path: 'study/:code',
        component: course.CourseStudyComponent
      },
      {
        path: 'home',
        component: course.CourseHomeComponent
      },
      {
        path: 'quiz/:code/:progress',
        component: course.CourseQuizComponent

      }
    ]
  },
  {
    path: 'user',
    canActivate: [ CanActivateViaAuthGuard ],
    component: user.UserComponent,
    children: [
      {
        path: '',
        component: user.AccountProfileComponent
      },
      {
        path: 'settings',
        component: user.SettingsProfileComponent,
        children: [
          {
            path: '',
            component: user.GeneralProfileComponent
          },
          {
            path: 'changePassword',
            component: user.ChangePasswordComponent
          }
        ]
      },
      {
        path: 'statistic',
        component: user.StatisticProfileComponent
      },
      {
        path: 'plans',
        component: user.PlansProfileComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/fourohfour',
    pathMatch: 'full'
  },
];

export const appRoutingProviders: any[] = [
  CanActivateViaAuthGuard
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);