import { Component, OnInit, ViewContainerRef } from "@angular/core";

import { Subscription } from 'rxjs/Subscription';

import { AuthenticationService } from '../../model/user/authentication.service';
import { ListSettingService } from '../../model/settings/listSetting.service';

const APP_VERSION: string = '1.0.0';

@Component({
  selector: "wordbrewery",
  template: `
      <layout></layout>
      <!-- Routed views go here -->
      <router-outlet></router-outlet>
    `
})
export class WordbreweryComponent {
    private _subs: Array<Subscription> = [];
    private version: string = APP_VERSION;

  constructor(
    private userSvc: AuthenticationService,
    private listSetting: ListSettingService,
    private viewContainerRef: ViewContainerRef
  ) {
    //this.userSvc.getUser();
  }

  ngOnInit() {
      this.listSetting.loadDefaultSettings();
  }
}
