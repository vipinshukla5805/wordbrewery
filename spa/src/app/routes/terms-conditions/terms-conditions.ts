import { Component, OnInit, NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import CommonFooterModule from "../../comp/footer/common-footer";

@Component({
    templateUrl: '/app/routes/terms-conditions/terms-conditions.html'
})

export class TermsConditionsComponent {
}

@NgModule({
    declarations : [
        TermsConditionsComponent
    ],
    imports : [
        CommonFooterModule
    ]
})
export default class TermsConditionsModule {}
