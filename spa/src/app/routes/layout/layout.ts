import { Component, NgModule, Input, Inject } from "@angular/core";
import { Location } from '@angular/common';
import { RouterLink, Router, NavigationStart, NavigationEnd } from '@angular/router';

import { Subscription } from 'rxjs/Subscription'

import {
  AuthenticationService,
  IUserData,
  IProfileData,
  IUserResponse,
  IUserStatisticData
} from '../../model/user/authentication.service'
import { SettingsService, EU_COUNTRIES } from '../../model/settings/settings.service'
import { TranslatorService } from '../../model/translator/translator.service'
import { AudioService } from '../../model/core/audio'
import { Truncate } from '../../pipes/truncate.pipe'

import { CommonFooterComponent } from '../../comp/footer/common-footer'

import { POPOVER_DIRECTIVES } from "../../comp/popover/popover";

export const COMPONENTS = [
  POPOVER_DIRECTIVES,
  Truncate
];

declare var ga: any, fbq: any;
declare var gapi: any, FB: any;

@Component({
  selector: "layout",
  templateUrl: '/app/routes/layout/layout.html'
})
export class LayoutComponent {
  private _subs: Array<Subscription> = []
  private userData: IUserData;
  private userProfileData: IProfileData;
  private hops: number;
  private currentRoute: string = null;
  private myTarget: string;
  private myTarget2: string;
  private clicked: string;

  constructor(
    private userSvc: AuthenticationService,
    private router: Router,
    private settingSvc: SettingsService,
    private translateSvc: TranslatorService,
    private audio: AudioService,
    private location: Location
  ) {
  }

  ngOnInit() {
    this._subs.push(
      this.userSvc.userData$.subscribe(data => this.userData = data)
    );

    this._subs.push(
      this.userSvc.userProfileData$.subscribe(data => this.userProfileData = data)
    );

    this._subs.push(
      this.router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
          this.audio.pause()
          let newRoute = this.location.path() || '/';
          if (newRoute !== this.currentRoute) {
            // set google analytics page.
            ga('send', 'pageview', newRoute);
            this.userSvc.getUser();
            this._subs.push(
              this.userSvc.getHops()
                .subscribe((res: IUserResponse) => {
                  this.hops = res.status ? res.data : null;
                }));
            //fbq('track', 'PageView', newRoute);
            this.currentRoute = newRoute;
          }
        }
        else if (event instanceof NavigationEnd) {
          // hide menu
          document.querySelector('.animenu__toggle').classList.remove('animenu__toggle--active');
          document.querySelector('.animenu__nav').classList.remove('animenu__nav--open');
        }
      }));

    this.translateSvc.getSupportedLanguageForSpeak();
  }

  ngAfterViewInit() {
    this.toggleNavigation()
    this.backToTop()
    setTimeout(() => this.initCookieAgreementModal(), 3000);
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe();
  }

  saveUserAgreement() {
    $('#cookieModal').removeClass('is-visible')
    this.settingSvc.saveUserCookieAgreement(true)
      .subscribe((res) => {

      })
  }

  logout() {
    this.userSvc.logout()
      .subscribe((res: IUserResponse) => {
        if (res.status) {
          this.userSvc.isLoggedIn$.next(false);
          this.userSvc.userData$.next(null);
          this.userSvc.userProfileData$.next(null);
          this.router.navigate([''])
        }
      })
  }

  private toggleNavigation() {
    $(document).off('click', '.animenu__toggle')
      .on('click', '.animenu__toggle', function (e) {
        e.preventDefault();
        $(this).toggleClass('animenu__toggle--active')
        $('.animenu__nav').toggleClass('animenu__nav--open')
      });
  }

  private backToTop() {
    var offset = 300,
      offset_opacity = 1200,
      scroll_top_duration = 700,
      $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function () {
      ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible')
        : $back_to_top.removeClass('cd-is-visible cd-fade-out');
      if ($(this).scrollTop() > offset_opacity) {
        $back_to_top.addClass('cd-fade-out');
      }
    });

    //smooth scroll to top
    $back_to_top.on('click', function (event) {
      event.preventDefault();
      $('body, html').animate({
        scrollTop: 0,
      }, scroll_top_duration
      );
    });
  }

  initCookieAgreementModal() {
    this.settingSvc.getUserGeoLocation()
      .subscribe(res => {
        if (res && EU_COUNTRIES.indexOf(res.country_name) > -1
          && !this.settingSvc.getUserCookieAgreement()) {
          $('#cookieModal').addClass('is-visible')
        }
      })
  }
}
