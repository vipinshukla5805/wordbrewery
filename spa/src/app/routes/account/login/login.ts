import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {
  FormBuilder, Validators, FormControl, FormGroup, ReactiveFormsModule
} from '@angular/forms';

import { Router, RouterLink } from '@angular/router'

import { Subscription } from 'rxjs/Subscription'

import { AccountValidator } from '../validator'
import { AuthenticationService, IUserResponse } from '../../../model/user/authentication.service';
import ShowHideContainer from '../../../comp/show-hide-pwd/show-hide-container';

import SocialLoginModule from '../../../comp/social-login'

import { ControlMessages } from '../control-message'

@Component({
  templateUrl: '/app/routes/account/login/login.html'
})
export class LoginComponent {
  private loginForm: FormGroup;
  private errorMessage: string;
  private isLoading: boolean = false;

  private _subs: Array<Subscription> = [];

  constructor(
    private formBuilder: FormBuilder,
    private userService: AuthenticationService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      'userEmail': [ '', Validators.compose([ Validators.required, AccountValidator.emailValidator ]) ],
      'userPassword': [ '', Validators.required ]
    });
  }

  private onPostLogin(res: any) {
    this.isLoading = false
    if (res.status) {
      sessionStorage.setItem('user', JSON.stringify(res.data));
      this.router.navigateByUrl('/explore');
    }
    else {
      this.errorMessage = res.message;
    }
  }

  login() {
    if (this.isLoading) return;
    this.errorMessage = '';
    
    if (this.loginForm.dirty && this.loginForm.valid) {
      this.isLoading = true;

      this._subs.push(
        this.userService.login(this.loginForm.value.userEmail.trim(), this.loginForm.value.userPassword.trim())
          .subscribe((res: IUserResponse) => {
            this.onPostLogin(res)
          }));
    }
  }

  onFacebookLogin(res: any) {
    this.isLoading = true;
    this.userService.doFacebookLogin(res.data)
      .subscribe((res: IUserResponse) => {
        this.onPostLogin(res)
      });
  }

  onGoogleLogin(token) {
    this.isLoading = true;
    this._subs.push(
      this.userService.doGoogleLogin(token)
        .subscribe((res: IUserResponse) => {
          this.onPostLogin(res)
        }));
  }

  ngOnDestroy() {
    while (this._subs.length) {
      this._subs.pop().unsubscribe();
    }
  }
}

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    ShowHideContainer,
    SocialLoginModule,
    ControlMessages
  ]
})
export class LoginModule { }
