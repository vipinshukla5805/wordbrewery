import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule, Router } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

import { UserService, IUserResponse } from '../../../model/user/authentication.service'

@Component({
  template: `
      <h5 class="text-center" 
      *ngIf="isLoading">
      <i class="fa fa-lg fa-refresh fa-spin"></i>
      </h5>
      <h5 class="text-center"
      [ngClass]="{'text-danger' : !responseStatus, 'text-success': responseStatus}" >{{msg}}</h5>
    `
})

export class ActivateComponent {
  private _subs: Array<Subscription> = [];
  private isLoading: boolean = false;
  private msg: string = '';
  private responseStatus: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userSvc: UserService
  ) {
  }

  ngOnInit() {
    this._subs.push(
      this.route.params.subscribe(param => {
        this.activateUser(param[ 'email' ], param[ 'code' ])
      })
    );
  }

  activateUser(email: string, code: string) {
    this.isLoading = true;

    this._subs.push(
      this.userSvc.activate(email, code)
        .subscribe((res: IUserResponse) => {
          this.msg = res.message;
          this.responseStatus = res.status;

          this.router.navigateByUrl('/explore');
        },
        null,
        () => this.isLoading = false)
    );
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe();
  }
}

@NgModule({
  declarations: [
    ActivateComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class ActivateModule { }
