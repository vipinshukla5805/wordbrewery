import { NgModule, Component } from '@angular/core';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  templateUrl: '/app/routes/account/account.html'
})
export class AccountComponent {
  constructor(private router: Router) { }

  private isTabEnable(): boolean {
    return this.router.isActive('account/login', true) || this.router.isActive('account/register', true);
  }
}

@NgModule({
  declarations: [
    AccountComponent
  ],
  imports: [
    RouterModule,
    CommonModule
  ]
})
export class AccountModule { }
