import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule
} from '@angular/forms';
import { Router, RouterModule } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

import { AccountValidator } from '../validator'
import { AuthenticationService, IUserResponse } from '../../../model/user/authentication.service'

import { ControlMessages } from '../control-message'

@Component({
  templateUrl: '/app/routes/account/forgot-password/forgot-password.html'
})

export class ForgotPasswordComponent {
  private _subs: Array<Subscription> = [];
  private isLoading: boolean = false;
  private response: IUserResponse;
  private forgotPwdForm: FormGroup;
  private errorMessage: string;

  constructor(
    private formBuilder: FormBuilder,
    private userService: AuthenticationService
  ) {
    this.forgotPwdForm = this.formBuilder.group({
      'userEmail': [ '', Validators.compose([ Validators.required, AccountValidator.emailValidator ]) ]
    });
  }

  forgotPassword() {
    if (this.forgotPwdForm.dirty && this.forgotPwdForm.valid) {
      this.isLoading = true;
      this._subs.push
        (
        this.userService.forgotPassword(this.forgotPwdForm.controls[ 'userEmail' ].value.trim())
          .subscribe((res: IUserResponse) => {
            this.response =  res
          }, null, () => this.isLoading = false)
        )
    }
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe();
  }
}

@NgModule({
  declarations: [
    ForgotPasswordComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    ControlMessages
  ]
})
export class ForgotPasswordModule { }
