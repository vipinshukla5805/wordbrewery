import { Component, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { FormControl, ReactiveFormsModule, FormsModule } from '@angular/forms'

import { AccountValidator } from './validator'

@Component({
    selector: 'control-messages',
    template: `
        <div class="row" style="padding-bottom:10px;">
                    <div class="col-md-12">
        <div class="text-danger" *ngIf="errorMessage !== null">{{errorMessage}}</div>
        </div>
        </div>`
})
export class ControlMessagesComponent {
    @Input() control: FormControl;
    constructor() { }

    get errorMessage() {
        for (let propertyName in this.control.errors) {
            if (this.control.errors.hasOwnProperty(propertyName) && this.control.dirty) {
                return AccountValidator.getValidatorErrorMessage(propertyName, this.control.errors[ propertyName ]);
            }
        }
        return null;
    }
}

@NgModule({
    declarations: [
        ControlMessagesComponent
    ],
    exports: [
        ControlMessagesComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule
    ]
})
export class ControlMessages { }