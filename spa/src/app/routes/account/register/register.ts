import { NgModule, Component } from '@angular/core'
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  FormBuilder, Validators,
  AbstractControl, FormGroup, FormControl, ReactiveFormsModule
} from '@angular/forms'
import { Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription'

import { AccountValidator } from '../validator'
import { AuthenticationService, IUserResponse } from '../../../model/user/authentication.service';
import ShowHideContainer from '../../../comp/show-hide-pwd/show-hide-container';

import SocialLoginModule from '../../../comp/social-login'

import { ControlMessages } from '../control-message'

@Component({
  templateUrl: '/app/routes/account/register/register.html'
})

export class RegisterComponent {
  private isLoading: boolean = false;
  private registerForm: FormGroup;
  private errorMessage: string;
  private userEmail: AbstractControl;

  private _subs: Array<Subscription> = [];

  constructor(
    private formBuilder: FormBuilder,
    private userService: AuthenticationService,
    private router: Router
  ) {
    this.registerForm = this.formBuilder.group({
      'userEmail': [ '', Validators.compose([
        Validators.required,
        AccountValidator.emailValidator
      ])
      ],
      'userPassword': [ '', Validators.compose([
        Validators.required,
        AccountValidator.passwordValidator
      ]) ]
    });

    this.userEmail = this.registerForm.controls[ 'userEmail' ];

    this._subs.push(
      this.userEmail
        .valueChanges
        .debounceTime(550)
        .distinctUntilChanged()
        .subscribe((value: string) => {
          if (!this.userEmail.errors) {
            this.usernameTaken(value);
          }
        }));
  }

  private usernameTaken(value: string) {
    this._subs.push(
      this.userService.doesUserExist(value)
        .subscribe((data: IUserResponse) => {
          if (data.status) {
            this.userEmail.setErrors({ 'emailIsTaken': false });
          }
        }));
  }

  register() {
    if (this.registerForm.dirty && this.registerForm.valid) {
      this.isLoading = true;
      this.errorMessage = '';
      this._subs.push(
        this.userService.register(this.registerForm.value.userEmail.trim(), this.registerForm.value.userPassword.trim())
          .subscribe((res: IUserResponse) => {
            if (res.status) {
              this.router.navigateByUrl(`/account/register/${this.registerForm.value.userEmail}`)
            }
            this.errorMessage = res.message;
          },
          null,
          () => this.isLoading = false));
    }
  }

  private onPostLogin(res: any) {
    this.isLoading = false
    if (res.status) {
      this.userService.getUser();

      this.router.navigate([ '/explore' ]);
    }
    else {
      this.errorMessage = res.message;
    }
  }


  onFacebookLogin(res: any) {
    this.isLoading = true;
    this._subs.push(
      this.userService.doFacebookLogin(res.data)
        .subscribe((res: IUserResponse) => {
          this.onPostLogin(res)
        }));
  }

  onGoogleLogin(token) {
    this.isLoading = true;
    this._subs.push(
      this.userService.doGoogleLogin(token)
        .subscribe((res: IUserResponse) => {
          this.onPostLogin(res)
        }));
  }

  ngOnDestroy() {
    while (this._subs.length) {
      this._subs.pop().unsubscribe();
    }
  }
}

@NgModule({
  declarations: [
    RegisterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    ShowHideContainer,
    SocialLoginModule,
    ControlMessages
  ]
})
export class RegisterModule { }
