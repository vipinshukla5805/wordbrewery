import { Injectable } from '@angular/core'
import { FormControl } from '@angular/forms'

export interface IValidationResult {
  [key: string]: boolean;
}

export class AccountValidator {
  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
      'required': 'This field is required.',
      'currentPassword': 'This password does not match with our records.',
      'samePassword': 'New password must not match to current one.',
      'passwordsAreNotEqual': 'Password does not match.',
      'emailIsTaken': 'This email address is taken.',
      'invalidEmailAddress': 'Invalid email address.',
      'invalidPassword': 'Passwords must be between 4 and 12 characters long and contain at least one letter and one number.',
      'minlength': `Minimum length ${validatorValue.requiredLength}`
    };

    return config[validatorName];
  }

  static emailValidator(control: FormControl): IValidationResult {
    // RFC 2822 compliant regex
    if (control.value &&
      !control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
      return { 'invalidEmailAddress': false };
    }
    return null;
  }

  static passwordValidator(control: FormControl): IValidationResult {
    let letter = /[a-zA-Z]/;
    let number = /[0-9]/;
    let valid = number.test(control.value) && letter.test(control.value)
      && control.value.length >= 4 && control.value.length <= 12;
    if (!valid) {
      return { "invalidPassword": false };
    }
  }
}
