import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import { UserService } from '../../../model/user/user.service';

@Component({
  templateUrl: '/app/routes/account/account-created/acc-created.html'
})

export class AccountCreatedComponent {
  private _subs: Array<Subscription> = []
  private email: string;

  constructor(
    route: ActivatedRoute,
    private userSvc: UserService
  ) {
    this.email = route.snapshot.params[ 'email' ];
  }

  sendConfirmation(email: string) {
    if (!email) return;

    this.userSvc.resend(email)
      .subscribe(data => {

      })
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe()
  }
}

@NgModule({
  declarations: [
    AccountCreatedComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class AccountCreatedModule { }
