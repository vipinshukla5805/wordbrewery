import { Component, OnInit, NgModule } from "@angular/core";
import CommonFooterModule from "../../comp/footer/common-footer";

@Component({
    templateUrl: '/app/routes/about/about.html'
})
export class AboutComponent {}

@NgModule({
    declarations : [
        AboutComponent
    ],
    imports: [
        CommonFooterModule
    ]
})
export default class About {}
