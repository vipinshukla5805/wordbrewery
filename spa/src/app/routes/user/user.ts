import { Component, NgModule } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

import { AuthenticationService, IUserResponse } from '../../model/user/authentication.service';

@Component({
  templateUrl: '/app/routes/user/user.html'
})

export class UserComponent {
  constructor(
    private router: Router,
    private userSvc: AuthenticationService
  ) {
  }

  logout() {
    this.userSvc.logout()
      .subscribe((res: IUserResponse) => {
        if (res.status) {
          this.userSvc.isLoggedIn$.next(false);
          this.userSvc.userData$.next(null);
          this.userSvc.userProfileData$.next(null);
          this.router.navigateByUrl('/')
        }
      });
  }
}

@NgModule({
  imports: [ RouterModule ],
  declarations: [ UserComponent ],
  exports: [ UserComponent ]
})
export class UserModule { }
