import { Component, Input, NgModule } from '@angular/core';
import { Router, RouterModule } from "@angular/router";

import { Subscription } from 'rxjs/Subscription';

import { AuthenticationService, IUserData, IProfileData, IUserResponse } from '../../../../model/user/authentication.service';

interface ITimelineData {
  value: string;
  langauge: string;
  level: string;
  type: string;
}

@Component({
  templateUrl: '/app/routes/user/profile/account/accountProfile.html'
})
export class AccountProfileComponent {
  private _subs: Array<Subscription> = [];
  private userData: IUserData;
  private userProfileData: IProfileData;
  private hops: number;

  private timelineData: Array<ITimelineData> = [];

  constructor(
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this._subs.push(
      this.authenticationService.userData$.subscribe((data: IUserData) => this.userData = data)
    );
    this._subs.push(
      this.authenticationService.userProfileData$.subscribe((data: IProfileData) => this.userProfileData = data)
    );
    this._subs.push(
      this.authenticationService.getHops()
        .subscribe((res: IUserResponse) => {
          this.hops = res.status ? res.data : 0;
        }));
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe()
  }
}

@NgModule({
  declarations: [ AccountProfileComponent ],
  exports: [ AccountProfileComponent ]
})
export class AccountProfileModule { }