import { Component, Input, NgModule } from '@angular/core';
import { Router, RouterModule } from "@angular/router";

@Component({
    templateUrl: '/app/routes/user/profile/statistics/statisticsProfile.html'
})
export class StatisticProfileComponent { }

@NgModule({
  declarations: [ StatisticProfileComponent ],
  exports: [ StatisticProfileComponent ]
})
export class StatisticProfileModule { }