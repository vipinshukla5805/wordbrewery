import { Component, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroup, FormsModule, FormControl, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from "@angular/router";

import { Subscription } from 'rxjs/Subscription';

import { AuthenticationService, IProfileData, IUserResponse } from '../../../../../model/user/authentication.service';
import { ListSettingService } from '../../../../../model/settings/listSetting.service';
import { RequestStatus } from '../../../../../model/requestStatus.class';

import { ControlMessages } from '../../../../account/control-message';

import { ProfileService } from '../../model/profile.service'

@Component({
  templateUrl: '/app/routes/user/profile/settings/general/general.html'
})
export class GeneralProfileComponent {
  private generalFrom: FormGroup;
  private _subs: Array<Subscription> = [];
  private userData: IProfileData;
  private dateOfBirth: Date;
  private requestStatus: RequestStatus = new RequestStatus();

  constructor(
    private fb: FormBuilder,
    private listSvc: ListSettingService,
    private authSvc: AuthenticationService,
    private profileSvc: ProfileService
  ) {
  }

  ngOnInit() {
    this._subs.push(
      this.authSvc.userProfileData$.subscribe((res: IProfileData) => {
        this.userData = res;
        this.initForm();
      })
    )
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe();
  }

  private initForm() {
    if (this.userData.yearOfBirth && this.userData.monthOfBirth && this.userData.dayOfBirth) {
      this.dateOfBirth = new Date(this.userData.yearOfBirth,
        (this.userData.monthOfBirth - 1), this.userData.dayOfBirth);
    }
    this.generalFrom = this.fb.group({
      'userName': new FormControl(this.userData.userName),
      'location': new FormControl(this.userData.location),
      'nativeLanguage': new FormControl(this.userData.nativeLanguage),
      'dob': new FormControl(this.getISOString())
    });
  }

  private getISOString() {
    let month = (this.dateOfBirth.getMonth() + 1);
    let date = this.dateOfBirth.getDate();

    return this.dateOfBirth ? `${this.dateOfBirth.getFullYear()}-${month < 10 ? '0' + month : month}-${date < 10 ? '0' + date : date}` : '';
  }

  private saveGeneralData() {
    if (this.generalFrom.dirty && this.generalFrom.valid) {
      this.requestStatus = {
        isServerRequesting: true,
        isError: false,
        isSuccess: false
      };

      let dob = new Date(this.generalFrom.controls[ 'dob' ].value.trim());
      let userObj = Object.assign({}, this.userData, {
        userName: this.generalFrom.controls[ 'userName' ].value.trim(),
        location: this.generalFrom.controls[ 'location' ].value.trim(),
        nativeLanguage: this.generalFrom.controls[ 'nativeLanguage' ].value.trim(),
        dayOfBirth: dob.getDate(),
        monthOfBirth: dob.getMonth() + 1,
        yearOfBirth: dob.getFullYear()
      });

      this._subs.push(
        this.profileSvc.updateProfileUser(userObj).subscribe((res: IUserResponse) => {
          this.requestStatus = {
            isServerRequesting: false,
            isError: false,
            isSuccess: res.status
          };
          this.authSvc.getProfileUser(true);
        }, () => {
          this.requestStatus = {
            isServerRequesting: false,
            isError: true,
            isSuccess: false
          };
        }));
    }
  }
}

@NgModule({
  declarations: [ GeneralProfileComponent ],
  exports: [ GeneralProfileComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ControlMessages
  ],
  providers: [
    ProfileService
  ]
})
export class GeneralProfileModule { }