import { Component, Input, NgModule } from '@angular/core';
import { Router, RouterModule } from "@angular/router";

@Component({
    templateUrl: '/app/routes/user/profile/settings/settingsProfile.html'
})

export class SettingsProfileComponent {
 constructor(private router: Router) { }
}

@NgModule({
  declarations: [ SettingsProfileComponent ],
  imports: [ RouterModule ],
  exports: [ SettingsProfileComponent ]
})
export class SettingsProfileModule { }