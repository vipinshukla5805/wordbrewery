import { Component, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";

import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription'
import { Observable } from 'rxjs/Observable'
import { Subject } from 'rxjs/Subject'
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { AuthenticationService, IUserData, IUserResponse } from '../../../../../model/user/authentication.service';
import { RequestStatus } from '../../../../../model/requestStatus.class';

import { AccountValidator } from '../../../../account/validator'

import { ControlMessages } from '../../../../account/control-message'

import { ProfileService } from '../../model/profile.service'

@Component({
  templateUrl: '/app/routes/user/profile/settings/changePassword/changePassword.html'
})
export class ChangePasswordComponent {
  private changePasswordFrom: FormGroup;
  private currentPwd: AbstractControl;
  private newPwd: AbstractControl;
  private confirmNewPwd: AbstractControl;
  private requestStatus: RequestStatus = new RequestStatus();
  private _subs: Array<Subscription> = [];

  constructor(
    private authenticationService: AuthenticationService,
    private fb: FormBuilder,
    private profileService: ProfileService
  ) {
    this.changePasswordFrom = fb.group({
      'currentPwd': [ '', Validators.required ],
      'newPwd': [ '', Validators.compose([ Validators.required, AccountValidator.passwordValidator ]) ],
      'confirmNewPwd': [ '', Validators.compose([ Validators.required, AccountValidator.passwordValidator ]) ]
    });

    this.currentPwd = this.changePasswordFrom.controls[ 'currentPwd' ];
    this.newPwd = this.changePasswordFrom.controls[ 'newPwd' ];
    this.confirmNewPwd = this.changePasswordFrom.controls[ 'confirmNewPwd' ];

    this._subs.push(
      this.currentPwd
        .valueChanges
        .debounceTime(550)
        .distinctUntilChanged()
        .subscribe(res => {
          this.authenticationService.getUser();
          this.authenticationService.userData$.subscribe((res: IUserData) => {
            if (res && this.currentPwd.value) {
              this.validateCurrentPassword(res.email, this.currentPwd.value)
                .then(res => {
                  this.currentPwd.setErrors(res);
                })
            }
          })
        }));

    this._subs.push(
      this.newPwd
        .valueChanges
        .debounceTime(550)
        .distinctUntilChanged()
        .subscribe(_ => {
          this.comparePasswords()
        }));

    this._subs.push(
      this.confirmNewPwd
        .valueChanges
        .debounceTime(550)
        .distinctUntilChanged()
        .subscribe(_ => {
          this.comparePasswords()
        }));
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe();
  }

  private comparePasswords(): any {
    let passwordsAreNotEqual = this.newPwd.value !== this.confirmNewPwd.value
    if (passwordsAreNotEqual)
      this.confirmNewPwd.setErrors({ 'passwordsAreNotEqual': passwordsAreNotEqual });
    this.validateCurrentAndNewPassword(this.currentPwd.value, this.newPwd.value)
      .then(res => {
        this.newPwd.setErrors(res);
      })
  }

  private validateCurrentPassword(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this._subs.push(
        this.profileService.validatePassword(email, password)
          .subscribe(res => {
            if (!res.status) {
              resolve({ currentPassword: true })
            } else {
              resolve(null);
            }
          }, () => reject(null)));
    })
  }

  private validateCurrentAndNewPassword(currentPwd: string, newPwd: string) {
    return new Promise((resolve, reject) => {
      let isNewPasswordSame = currentPwd === newPwd;
      if (isNewPasswordSame) {
        resolve({ samePassword: true })
      } else {
        resolve(null);
      }
    })
  }

  private changePassword() {
    if (this.changePasswordFrom.dirty && this.changePasswordFrom.valid) {
      this.requestStatus = {
        isServerRequesting: true,
        isError: false,
        isSuccess: false
      };

      this._subs.push(
        this.authenticationService.resetPassword(this.newPwd.value)
          .subscribe((res: IUserResponse) => {
            if (res.status) {
              this.requestStatus.isSuccess = true;
              this.changePasswordFrom.reset();
            }
            this.requestStatus.isServerRequesting = false;
          }, () => {
            this.requestStatus.isError = true;
            this.requestStatus.isServerRequesting = false;
          }));
    }
  }
}

@NgModule({
  declarations: [ ChangePasswordComponent ],
  exports: [ ChangePasswordComponent ],
  imports: [ ReactiveFormsModule, CommonModule, ControlMessages ],
  providers: [ ProfileService ]
})
export class ChangePasswordModule { }