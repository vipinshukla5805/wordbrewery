import { Component, Input, NgModule } from '@angular/core';

@Component({
  templateUrl: '/app/routes/user/profile/plans/plansProfile.html'
})
export class PlansProfileComponent { }

@NgModule({
  declarations: [ PlansProfileComponent ],
  exports: [ PlansProfileComponent ]
})
export class PlansProfileModule { }