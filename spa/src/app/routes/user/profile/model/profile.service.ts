import { Injectable } from '@angular/core';
import { Http, RequestMethod } from '@angular/http';

import { httpRequest } from '../../../../model/core/httpRequest';

export interface IProfileResponse {
  status: string;
  data: any;
  message: string;
}

@Injectable()
export class ProfileService {
  constructor(private http: Http) { }

  getProfileTimeline() {
    let req = {
      url: '',
      method: RequestMethod.Get
    };

    return httpRequest(this.http, req);
  }

  validatePassword(userEmail: string, userPassword: string) {
    let params = `userEmail=${userEmail}&userPassword=${userPassword}`;
    let req = {
      url: '/api/user/password/validate',
      method: RequestMethod.Get,
      search: params
    };

    return httpRequest(this.http, req);
  }

  updateProfileUser(user: any) {
    let req = {
      url: '/api/user/profile',
      method: RequestMethod.Post,
      body: JSON.stringify(user)
    };

    return httpRequest(this.http, req);
  }
}