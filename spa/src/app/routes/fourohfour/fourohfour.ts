import { Component, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import CommonFooterModule from '../../comp/footer/common-footer'

@Component({
  templateUrl: '/app/routes/fourohfour/fourohfour.html'
})

export class FourOhFourComponent {
}

@NgModule({
  declarations: [
    FourOhFourComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    CommonFooterModule
  ]
})
export default class FourOhFour { }
