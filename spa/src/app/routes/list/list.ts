import { Component, NgModule, ViewChild } from '@angular/core'
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RouterModule, Router} from "@angular/router";

import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription';

import { ModalModule, ModalDirective } from 'ng2-bootstrap/ng2-bootstrap';

import * as common from '../../model/core/common';

import { SettingsService } from '../../model/settings/settings.service';
import { ListSettingService } from '../../model/settings/listSetting.service';
import { WordListService } from '../../model/lists/wordList.service'
import { WordList } from '../../model/lists/wordList.class'
import { SentenceListService} from '../../model/lists/sentenceList.service'
import { SentenceList } from '../../model/lists/sentenceList.class'
import { ClientLanguage } from '../../model/settings/clientLanguage.class'
import DropdownModule, { DropdownComponent, IDropdownOption } from '../../comp/dropdown/dropdown'
import { RequestStatus } from '../../model/requestStatus.class';
import CommonFooterModule from '../../comp/footer/common-footer'


export enum EnumListType {
  Word = 1,
  Sentence = 2
}

@Component({
  templateUrl: '/app/routes/list/list.html'
})
export class ListComponent {
  @ViewChild('modal') listModal: ModalDirective;

  _subs: Array<Subscription> = []
  sortingOptions: Array<IDropdownOption> = common.SORTING_OPTIONS;
  languages: Array<ClientLanguage> = [];
  sentenceList: Array<SentenceList> = [];
  wordList: Array<WordList> = [];
  requestStatus: RequestStatus = new RequestStatus();
  private clickedListData: any;
  private settings: any;

  constructor(
    private wordListSvc: WordListService,
    private sentenceListSvc: SentenceListService,
    private settingSvc: SettingsService,
    private listSettingService: ListSettingService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this._subs.push(
      this.listSettingService.listSettings$
        .subscribe((data) => {
          if (!data) return;

          this.settings = data.settings;
          this.languages = data.listSettings.languages

          //As per carlee's request
          this.languages.unshift(<any>{
            text: 'All',
            value: '',
            'class': 'flag-icon ',
            selected: true,
            originalItem: null
          })

          let lang = <any>this.languages[ 0 ];
          this.loadListByLanguage(lang.value);
        })
    );
  }

  private getImageLink(languageId: string) {
    let imageNumber: number = Math.floor(Math.random() * 5 + 1);
    return `images/lists/${languageId}/${imageNumber}.jpg`;
  }

  private getCountryCodeByLanguage(language) {
    if (this.languages && language) {
      let country = this.languages.filter((data) => data.languageId && data.languageId.indexOf(language) > -1)
      return country && country.length > 0 ? country[ 0 ].countryCode : '';
    }
  }

  private getCountryFlag(language) {
    if (this.languages && language) {
      let flag: any = this.languages.filter((data: any) => data.originalItem &&
        data.originalItem.languageId.indexOf(language) > -1)
      return flag && flag.length > 0 ? flag[ 0 ].class : '';
    }
  }

  private getPercentage(current: number, total: number) {
    return Math.round(100 * current / total)
  }

  /* Sorting Methods */

  private sortByDate() {
    this.sentenceList = <any>common.sortByKey(this.sentenceList, 'dateCreated');
    this.wordList = <any>common.sortByKey(this.wordList, 'dateCreated');
  }

  private sortByName() {
    this.sentenceList = <any>common.sortByKey(this.sentenceList, 'dateCreated');
    this.wordList = <any>common.sortByKey(this.wordList, 'dateCreated');
  }

  private sortByLanguage() {
    this.sentenceList = <any>common.sortByKey(this.sentenceList, 'languageId');
    this.wordList = <any>common.sortByKey(this.wordList, 'languageId');
  }

  private sortBySize() {
    this.sentenceList = <any>common.sortByKey(this.sentenceList, 'total');
    this.wordList = <any>common.sortByKey(this.wordList, 'total');
  }

  private sortByProgress() {
    this.sentenceList = <any>common.sortByKey(this.sentenceList, (obj) => this.getPercentage(obj.learned, obj.total));
    this.wordList = <any>common.sortByKey(this.wordList, (obj) => this.getPercentage(obj.learned, obj.total));
  }

  private sortByLeastLearned() {
    this.sentenceList = <any>common.sortByKey(this.sentenceList, 'learned');
    this.wordList = <any>common.sortByKey(this.wordList, 'learned');
  }

  private sortByMostLearned() {
    this.sortByLeastLearned();

    let sentences: any = this.sentenceList[ 0 ];
    let words: any = this.wordList[ 0 ];

    this.sentenceList = [ sentences.reverse() ];
    this.wordList = [ words.reverse() ];
  }

  /* End of Sorting Methods */

  private onLanguageChange(data) {
    this.cancelSubscription()
    this.loadListByLanguage(data ? data.languageId : null)
  }

  private loadLists() {
    this.sentenceList = [];
    this.wordList = [];
    this.requestStatus.isServerRequesting = true;

    this._subs.push(
      Observable.forkJoin(
        this.sentenceListSvc.getlists(),
        this.wordListSvc.getlists()
      )
        .subscribe((res: any) => {
          let sentences = res[ 0 ].map((item) => {
            item.imageLink = this.getImageLink(item.languageId)
            return item
          })

          let words = res[ 1 ].map((item) => {
            item.imageLink = this.getImageLink(item.languageId)
            return item
          })

          this.sentenceList = common.groupByRow(sentences);
          this.wordList = common.groupByRow(words);

          this.sortByDate();

        }, null, () => this.requestStatus.isServerRequesting = false)
    )
  }

  private loadListByLanguage(languageId: string = '') {
    if (!languageId) {
      this.loadLists();
    }
    else {
      this.sentenceList = [];
      this.wordList = [];
      this.requestStatus.isServerRequesting = true;

      this._subs.push(
        Observable.forkJoin(
          this.sentenceListSvc.getlistsByLanguage(languageId),
          this.wordListSvc.getlistsByLanguage(languageId)
        )
          .subscribe((res: any) => {
            let sentences = res[ 0 ].map((item) => {
              item.imageLink = this.getImageLink(item.languageId)
              return item
            })

            let words = res[ 1 ].map((item) => {
              item.imageLink = this.getImageLink(item.languageId)
              return item
            })

            this.sentenceList = common.groupByRow(sentences);
            this.wordList = common.groupByRow(words);

            this.sortByDate();

          }, null, () => this.requestStatus.isServerRequesting = false)
      )
    }
  }

  private cancelSubscription() {
    while (this._subs.length) {
      this._subs.pop().unsubscribe()
    }
  }


  private onSortChange(data): any {
    switch (parseInt(data.value)) {
      case common.EnumSortingOptions.Recent:
        this.sortByDate();
        break;

      case common.EnumSortingOptions.Name:
        this.sortByName();
        break;

      case common.EnumSortingOptions.Size:
        this.sortBySize();
        break;

      case common.EnumSortingOptions.Progress:
        this.sortByProgress();
        break;

      case common.EnumSortingOptions.Language:
        this.sortByLanguage();
        break;

      case common.EnumSortingOptions.Least_Learned:
        this.sortByLeastLearned();
        break;

      case common.EnumSortingOptions.Most_Learned:
        this.sortByMostLearned();
        break;
    }
  }

  private onListClick(data, type) {
    if (!data) return;

    if (!data.total) {
      this._subs.push(
        this.wordListSvc.getSuggestedWord(data.languageId, this.settings.currentLevel)
          .subscribe((res) => {
            this.clickedListData = Object.assign({}, data,
              { wordList: res.status && res.data && res.data.length > 0 ? res.data.slice(0, 3) : null });

            this.listModal.show();
          })
      );
    } else {
      this.router.navigateByUrl(`list/${data.id}/${type}`);
    }
  }
}

@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule,
    CommonFooterModule,
    DropdownModule
  ],
  providers: [
    WordListService,
    SentenceListService
  ]
})
export default class List { }