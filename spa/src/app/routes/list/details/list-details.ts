import { Component, NgModule, HostListener } from '@angular/core';
import { CommonModule } from "@angular/common";
import { RouterModule, Router, ActivatedRoute } from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
  FormsModule
} from '@angular/forms'

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { WordListService } from '../../../model/lists/wordList.service'
import { WordList } from '../../../model/lists/wordList.class'
import { SentenceListService } from '../../../model/lists/sentenceList.service'
import { SentenceList } from '../../../model/lists/sentenceList.class'
import { EnumListType } from '../list'
import { SpeakerService } from '../../../model/speaker/speaker.class'
import { SettingsService } from '../../../model/settings/settings.service'
import { RequestStatus } from '../../../model/requestStatus.class';
import { TranslatorService } from '../../../model/translator/translator.service';
import { DefinitionsService, IDefinition } from '../../../model/definitions/definitions.service';
import { Popovers } from '../../../model/popovers/popovers';
import CommonFooterModule from '../../../comp/footer/common-footer'

@Component({
  templateUrl: '/app/routes/list/details/list-details.html'
})
export class ListDetailsComponent {
  private _subs: Array<Subscription> = [];
  private listId: string;
  private listType: number;
  private list: any = {
    id: '',
    name: '',
    description: ''
  };
  private currentLevel: string;
  private language: any;

  private learnedList: any = [];
  private progressList: any = [];
  private requestStatus = new RequestStatus();

  private listForm: FormGroup;

  private listErrMsg: string = '';

  constructor(
    private wordListService: WordListService,
    private sentenceListService: SentenceListService,
    private router: Router,
    private route: ActivatedRoute,
    private speaker: SpeakerService,
    private settingSvc: SettingsService,
    private translatorService: TranslatorService,
    private popovers: Popovers,
    private definitionService: DefinitionsService,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this._subs.push(
      this.route.params.subscribe((param: any) => {
        if (param && param['id']) {
          this.currentLevel = this.settingSvc.extractLevel();
          this.language = this.settingSvc.extractLanguage();
          this.listId = param.id;
          this.listType = param.type;

          this.loadList();
        }
      }));
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe();
  }

  initValidation() {
    let name = this.list && this.list.name;
    let desc = this.list && this.list.description;

    this.listForm = this.fb.group({
      'listName': new FormControl(name, Validators.required),
      'listDescription': new FormControl(desc)
    });
  }

  loadList() {
    if (this.listType == EnumListType.Word) {
      this.loadWordList()
      this.loadWordEntry()
    }
    else {
      this.loadSentenceList()
      this.loadSentenceEntry()
    }
  }

  private loadWordList() {
    this.requestStatus.isServerRequesting = true;
    this.wordListService.getList(this.listId)
      .subscribe(res => {
        this.list = res;
        this.initValidation();
      }, null, () => this.requestStatus.isServerRequesting = false)
  }

  private loadWordEntry() {
    this.requestStatus.isServerRequesting = true;

    this.wordListService.getEntries(this.listId)
      .subscribe(res => {
        if (res && res.length > 0) {
          this.learnedList = res.filter(res => res.isLearned);
          this.progressList = res.filter(res => !res.isLearned);
        }
      }, null, () => this.requestStatus.isServerRequesting = false)
  }



  private loadSentenceList() {
    this.requestStatus.isServerRequesting = true;
    this.sentenceListService.getList(this.listId)
      .subscribe(res => {
        this.list = res;
        this.initValidation();
      }, null, () => this.requestStatus.isServerRequesting = false)
  }

  private loadSentenceEntry() {
    this.requestStatus.isServerRequesting = true;

    this.sentenceListService.getEntries(this.listId)
      .subscribe(res => {
        if (res && res.length > 0) {
          this.learnedList = res.filter(res => res.isLearned);
          this.progressList = res.filter(res => !res.isLearned);
        }
      }, null, () => this.requestStatus.isServerRequesting = false)
  }

  private speak(text: string) {
    this.speaker.speak(text, this.list.languageId, this.currentLevel);
  }

  private markAsLearnedAndUnlearned(data: any, isLearned: boolean) {
    if (this.listType == EnumListType.Word) {
      this.wordListService.markLearned(this.list.id, data.id, isLearned)
        .subscribe((res) => {
          this.loadWordEntry()
        })
    }
    else {
      this.sentenceListService.markLearned(this.list.id, data.id, isLearned)
        .subscribe((res) => {
          this.loadSentenceEntry()
        })
    }
  }

  private showDefinitions(data: any, event: any) {
    this.popovers.hide();
    let target = <HTMLElement>event.target
    if (data.definitions && data.definitions.length > 0) {
      let content = '';

      data.definitions.slice(0, 3).forEach((item) => {
        content += '<p>- ' + item + '</p>'
      });

      target.setAttribute('data-placement', 'top')
      target.setAttribute('data-html', 'true')
      target.setAttribute('data-content', content)
      setTimeout(() => $(target).popover('show'), 300);
      return;
    }

    let params: IDefinition = {
      languageId: this.list.languageId,
      text: data.text,
      lemma: data.lemma
    }
    this.definitionService.getAndShowDefinitions(params, target);
  }

  private onMouseOver(data: any, event: any) {
    this.popovers.hide();

    let target = <HTMLElement>event.target
    if (data.translation) {
      target.setAttribute('data-placement', 'top')
      target.setAttribute('data-html', 'true')
      target.setAttribute('data-content', data.translation)
      setTimeout(() => $(target).popover('show'), 300);
      return;
    }
    this.translatorService.translate(data.text, this.language.shortLanguageCode, data.text, target);
  }
  private onMouseOvernew(data: any, event: any) {
    this.popovers.hide();

    let target = <HTMLElement>event.target
    if (data) {
      target.setAttribute('data-placement', 'top')
      target.setAttribute('data-html', 'true')
      target.setAttribute('data-content', data)
      setTimeout(() => $(target).popover('show'), 300);
      return;
    }

  }
  private onMouseOut() {
    this.popovers.hide();
  }

  private updateList() {
    this.listErrMsg = '';
    if (this.listForm.dirty && !this.listForm.valid) return false;
    
    let id = this.list.id;

    let name = this.listForm.controls[ 'listName' ].value ? this.listForm.controls[ 'listName' ].value.trim() : '';

    let description = this.listForm.controls[ 'listDescription' ].value ? this.listForm.controls[ 'listDescription' ].value.trim() : '';

    if (this.listType == EnumListType.Word) {
      this.wordListService.renameList(id, name, description)
        .subscribe(() => {
          $('#fmModal, #smModal').modal('hide');
          this.loadWordList();
        }, (errResp) => { 
          let err = errResp.json()
          this.listErrMsg = err.error;
          setTimeout(() => this.listErrMsg = '', 2000);
        })
    }
    else {
      this.sentenceListService.renameList(id, name, description)
        .subscribe(() => {
          $('#fmModal, #smModal').modal('hide');
          this.loadSentenceList();
        }, (errResp) => { 
          let err = errResp.json()
          this.listErrMsg = err.error;
          setTimeout(() => this.listErrMsg = '', 2000);
        })
    }
  }

  private removeList() {
    if (this.listType == EnumListType.Word) {
      this.wordListService.deleteList(this.list.id)
        .subscribe(() => {
          $('#fmModal, #smModal').modal('hide');
          this.router.navigate([ '/List' ])
        })
    }
    else {
      this.sentenceListService.deleteList(this.list.id)
        .subscribe(() => {
          $('#fmModal, #smModal').modal('hide');
          this.router.navigate([ '/List' ])
        })
    }
  }

  private removeEntry(entryId: string) {
    if (this.listType == EnumListType.Word) {
      this.wordListService.deleteEntry(this.list.id, entryId)
        .subscribe(() => {
          $('#fmModal, #smModal').modal('hide');
          this.loadWordEntry();
        })
    }
    else {
      this.sentenceListService.deleteEntry(this.list.id, entryId)
        .subscribe(() => {
          $('#fmModal, #smModal').modal('hide');
          this.loadSentenceEntry();
        })
    }
  }

  private exportToCSV() {
    let jsonArray = this.learnedList.concat(this.progressList);

    if (jsonArray.length == 0) return;

    jsonArray = jsonArray.map((data) => {
      return {
        text: data.text,
        translation: data.translation || 'No translation',
        examples: data.examples.join(',')
      }
    })

    let header = [ 'text', 'translation', 'examples' ];
    let csv = jsonArray.map(row => header.map(fieldName => JSON.stringify(row[ fieldName ] || '')))
    csv.unshift(header)
    csv = csv.join('\r\n').replace(/["']/g, "");

    let a: any = document.createElement('a');
    let blob = new Blob([ csv ], { 'type': 'application\/octet-stream' });
    let url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = `${this.list.name}.csv`;
    document.body.appendChild(a);
    a.click();
    setTimeout(() => {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 100);
  }

  @HostListener('document:click', [ '$event' ])
  public onClick($event: any): any {
    this.popovers.hide();
  }

  ngAfterViewInit() {
    this.popovers.init();
  }
}

@NgModule({
  declarations: [
    ListDetailsComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
    CommonFooterModule
  ],
  providers: [
    WordListService,
    SentenceListService
  ]
})
export default class ListDetails { }