import {
  Component, OnInit, AfterContentChecked, OnDestroy, NgModule, HostListener
} from '@angular/core';
import { CommonModule } from "@angular/common";
import { RouterModule, Router } from "@angular/router";
import {
  FormGroup, AbstractControl,
  Validators, FormBuilder,
  ReactiveFormsModule, FormsModule
} from '@angular/forms';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription'
import { Subject } from 'rxjs/Subject'

import { CarouselModule } from 'ng2-bootstrap/ng2-bootstrap';

import { ClientSentence } from '../../model/clientSentence.class';
import { AudioService } from '../../model/core/audio';
import { Word } from '../../model/word.class';
import { ReportedSentence } from '../../model/reportedSentence.class';
import { RequestStatus } from '../../model/requestStatus.class';
import { Popovers } from '../../model/popovers/popovers';

import { ClientLanguage, RTL_LANGUAGES } from '../../model/settings/clientLanguage.class';
import { ClientSettings } from '../../model/settings/clientSettings.class';
import { SettingsService } from '../../model/settings/settings.service';
import { AuthenticationService, IUserData, IUserStatisticData, IProfileData } from '../../model/user/authentication.service';
import { ListSettingService } from '../../model/settings/listSetting.service';

import providerSentenceBlender, { SentenceBlenderService } from './model/sentenceBlender.service'
import { ReportService } from '../../model/report/report.service'
import DropdownModule, { DropdownComponent, IDropdownOption } from '../../comp/dropdown/dropdown'
import StatisticsModule from '../../comp/statistics-counter'
import CommonFooterModule from '../../comp/footer/common-footer';

import { SentenceInfoBoxModule } from '../../comp/sentence-info-box';
import { WordInfoBoxModule } from '../../comp/word-info-box/word-info-box';

@Component({
  templateUrl: '/app/routes/explore/sentenceBlender.html'
})
export class SentenceBlenderComponent {
  private _subs: Array<Subscription> = [];
  private isRTL: boolean = false;
  private user: IProfileData;
  private sentences: ClientSentence[] = [];
  private wordSentence: ClientSentence;
  private settings: ClientSettings = new ClientSettings();
  private requestStatus: RequestStatus = new RequestStatus();
  private mainRequestStatus: RequestStatus = new RequestStatus();
  private aleternateRequestStatus: RequestStatus = new RequestStatus();
  private reportedSentence: ReportedSentence = new ReportedSentence();
  private ddlLanguages: Array<IDropdownOption> = []
  private ddlLevels: Array<IDropdownOption> = []
  private ddlCategories: Array<IDropdownOption> = []
  private isSuccess: boolean = false;
  private wordsMastered: IUserStatisticData;
  private wordsShown: IUserStatisticData;
  private sentencesShown: IUserStatisticData;
  private sentenceReported: string = '';
  private reportForm: FormGroup;
  private clickedWord: Word;

  private textChange$: Subject<any> = new Subject<any>();

  constructor(
    private reportService: ReportService,
    private settingsService: SettingsService,
    private popovers: Popovers,
    private userService: AuthenticationService,
    private sentenceBlenderService: SentenceBlenderService,
    private _fb: FormBuilder,
    private listSetting: ListSettingService,
    private audio: AudioService
  ) {
  }

  @HostListener('click', [ '$event' ])
  onClick(event: any): any {
    this.popovers.processDocumentOnClick();
  }

  ngAfterContentChecked(): any {
    this.popovers.init();
  }

  ngOnInit(): any {
    this.loadDefaultSettings();
    this.initNewListValidation();
    //code for search
  }

  ngOnDestroy() {
    this.cancelSubscription()
  }

  private initNewListValidation() {
    this.reportForm = this._fb.group({
      'reportText': [ '', Validators.required ]
    });
  }

  private load(): any {
    this.isRTL = RTL_LANGUAGES.indexOf(this.settings.currentLanguage.languageId) > -1;

    this.audio.pause();
    this.popovers.hide();

    this.mainRequestStatus.isServerRequesting = true;
    this.mainRequestStatus.isError = false;
    this.resetSentence();

    // loading user statistics
    this.getWordsMastered();
    this.getWordsShown();
    this.getSentenceShown();

    this._subs.push(
      this.sentenceBlenderService.getSentencePack(
        this.settings.currentLanguage.languageId,
        this.settings.currentLevel,
        this.settings.currentCategory
      )
        .subscribe(data => {
          this.popovers.hide();
          if (data && data[ 0 ]) {
            this.sentences = data;
          }
          this.mainRequestStatus.isError = false;
        },
        err => this.mainRequestStatus.isError = true,
        () => this.mainRequestStatus.isServerRequesting = false
        ));
  }

  private loadDefaultSettings(): any {
    this._subs.push(
      this.listSetting.listSettings$
        .subscribe(data => {
          if (!data) return;
          this.settings = data.settings;
          this.loadLists(data.listSettings);
          this.load();
          this.loadUser();
        }));
  }

  private getWordsMastered() {
    this.userService.getUserStatistics(
      this.settings.currentLevel,
      this.settings.currentLanguage.languageId,
      'WORDS_MASTERED')
      .subscribe((res: IUserStatisticData) => {
        this.wordsMastered = res.status ? res.data : null;
      })
  }

  private getWordsShown() {
    this.userService.getUserStatistics(
      this.settings.currentLevel,
      this.settings.currentLanguage.languageId,
      'WORDS_SHOWN')
      .subscribe((res: IUserStatisticData) => {
        this.wordsShown = res.status ? res.data : null;
      })
  }

  private getSentenceShown() {
    this.userService.getUserStatistics(
      this.settings.currentLevel,
      this.settings.currentLanguage.languageId,
      'SENTENCES_SHOWN')
      .subscribe((res: IUserStatisticData) => {
        this.sentencesShown = res.status ? res.data : null;
      })
  }

  private loadUser(): any {
    //this.userService.getUser();
    this.userService.userProfileData$.subscribe((res: IProfileData) => {
      this.user = res;
    });
  }

  private loadLists(settingData): any {
    this.ddlCategories = settingData.categories;
    this.ddlLanguages = settingData.languages;
    this.ddlLevels = settingData.levels;
  }

  private cancelSubscription() {
    while (this._subs.length) {
      this._subs.pop().unsubscribe()
    }
  }

  private resetSentence() {
    this.wordSentence = null;
    this.sentences = [];
    this.clickedWord = null;
  }

  private onChangeLanguage(lang: ClientLanguage): any {
    this.cancelSubscription();

    this.settings.currentLanguage = lang;
    this.reload();
  }

  private onChangeLevel(level: string): any {
    this.cancelSubscription();

    this.settings.currentLevel = level;
    this.reload();
  }

  private onChangeCategory(category: string): any {
    this.cancelSubscription();

    this.settings.currentCategory = category;
    this.reload();
  }

  private refresh(): any {
    this.cancelSubscription();

    this.load();
  }

  private reload(): any {
    this._subs.push(
      this.settingsService.setSettings(
        this.settings.currentLanguage.languageId,
        this.settings.currentLevel,
        this.settings.currentCategory
      ).subscribe(data => {
        this.listSetting.loadDefaultSettings(true);
      }));
    $('#modal-selected-language').modal('hide');

    this.load();
  }

  private report(): void {
    this.requestStatus.isServerRequesting = true;
    this.reportedSentence.reason = this.reportForm.controls[ 'reportText' ].value;

    this.reportService.report(this.reportedSentence)
      .subscribe((res) => {
        this.reportedSentence.reason = '';
        this.requestStatus.isSuccess = true;
        setTimeout(() => {
          $('#reportModal').removeClass('is-visible');
        }, 2000)
      },
      err => {
        this.requestStatus.isError = true;
      },
      () => {
        this.requestStatus.isServerRequesting = false;
        setTimeout(() => {
          this.requestStatus.isError = false;
          this.requestStatus.isSuccess = false;
        }, 2000);
      });
  }

  private reportDialog(sentence: any, event: any): any {
    this.sentenceReported = sentence.words
      .map((wd) => wd.text + (wd.isSpaceAfter ? ' ' : ''))
      .join('');
    this.reportedSentence = <ReportedSentence>{
      languageId: sentence.languageId,
      text: sentence.text,
      source: sentence.source,
      reportedByUser: this.user ? this.user.userName : ''
    }

    this.requestStatus = {
      isServerRequesting: false,
      isSuccess: false,
      isError: false
    }
    $('#reportModal').addClass('is-visible');
  }

  private closeReportModal() {
    $('.cd-popup').removeClass('is-visible');
  }

  private onAlternateSentenceWordClicked(data: any) {
    let { word, sentence } = data;

    this.sentences = [ sentence ];
    if (this.sentences.length > 0) {
      this.sentences[ 0 ].words.map(wd => wd.isHighlighted = (wd.lemma == word.lemma || wd.text == word.text))
    }
    this.wordSentence = null;
    this.clickedWord = word;
    this.mainRequestStatus.isServerRequesting = false;
    this.aleternateRequestStatus.isServerRequesting = true;

    // Added timeout to play audio first then load sentence as per Ryan's suggestion
    setTimeout(this.onWordMoreSentences(word), 1000);
  }

  private onWordClicked(data: any) {
    let { word, sentence } = data;
    this.wordSentence = null;
    this.clickedWord = word;
    this.mainRequestStatus.isServerRequesting = true;
    this.aleternateRequestStatus.isServerRequesting = false;
    // Added timeout to play audio first then load sentence as per Ryan's suggestion
    setTimeout(this.onWordMoreSentences(word), 1000);
  }

  private onWordMoreSentences(word: Word) {
    // highlight the clicked word
    if (this.aleternateRequestStatus.isServerRequesting) return;

    this.aleternateRequestStatus = {
      isServerRequesting: true,
      isError: false,
      isSuccess: false
    };

    let sentencesToFilter = this.sentences.map((sent) => {
      sent.words.map(wd => wd.isHighlighted = false)
      return sent.text;
    });
    word.isHighlighted = true;

    this._subs.push(
      this.sentenceBlenderService.searchSentencePack(
        this.settings.currentLanguage.languageId,
        this.settings.currentLevel, sentencesToFilter,
        word.text, word.lemma, word.pos)
        .subscribe((data) => {
          this.wordSentence = data[ 0 ];

          this.aleternateRequestStatus.isError = false;
          this.aleternateRequestStatus.isSuccess = true;
        },
        err => {
          this.aleternateRequestStatus.isSuccess = false
          this.aleternateRequestStatus.isError = true
        },
        () => this.aleternateRequestStatus.isServerRequesting = false
        ));
  }

  private searchSentencesBySearchText(searchText: string) {
    if (!searchText) return;

    this.cancelSubscription();

    this.popovers.hide();
    this.audio.pause();
    this.resetSentence();

    this.mainRequestStatus = {
      isServerRequesting: true,
      isSuccess: false,
      isError: false
    };

    let res = {
      text: searchText,
      level: this.settings.currentLevel,
      languageId: this.settings.currentLanguage.languageId
    };

    this.sentenceBlenderService.searchSentence(res.languageId, res.level, res.text)
      .subscribe(res => {
        this.sentences = res;
        this.mainRequestStatus = {
          isServerRequesting: false,
          isSuccess: true,
          isError: false
        };
      }, () => {
        this.mainRequestStatus = {
          isServerRequesting: false,
          isSuccess: false,
          isError: true
        };
      });
  }
}

@NgModule({
  declarations: [
    SentenceBlenderComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CommonFooterModule,
    CarouselModule,
    DropdownModule,
    StatisticsModule,
    SentenceInfoBoxModule,
    WordInfoBoxModule
  ],
  providers: [
    SentenceBlenderService
  ]
})
export default class SentenceBlenderModule { }