import { Component, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ActivatedRoute, Router, RouterModule } from "@angular/router";

import { Subscription } from 'rxjs/Subscription';

import CommonFooterModule from '../../../comp/footer/common-footer'
import DropdownModule, { DropdownComponent, IDropdownOption } from '../../../comp/dropdown/dropdown';
import providerCourseService, { CourseService, ICourseResponse, ICourseData } from '../model/course.service';

@Component({
  templateUrl: '/app/routes/courses/course-dashboard/course-dashboard.html'
})

export class CourseDashboardComponent {

  private _subs: Array<Subscription> = [];
  private CourseData: ICourseData;
  private courseCode: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    this._subs.push(
      this.route.params.subscribe((param: any) => {
        this.courseCode = param['code']; // (+) converts string 'id' to a number
        this._subs.push(
          this.courseService.getCourseData(this.courseCode)
            .subscribe((res: ICourseResponse) => {
              if (res.status == true && res.data) {
                this.CourseData = res.data[0];
              }
            }));
      }));
  }

  onContinue(courseCode) {

    this.router.navigateByUrl('/course/details/' + courseCode);
  }
}

@NgModule({
  declarations: [
    CourseDashboardComponent
  ],
  imports: [
    DropdownModule,
    RouterModule,
    CommonModule,
    CommonFooterModule
  ]
})
export class CourseDashboardModule { }
