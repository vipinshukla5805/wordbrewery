import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/interval'
import
{
    Injectable, NgModule
} from '@angular/core';
import
{
    Http, RequestMethod, Headers
} from '@angular/http';
import
{
    httpRequest
} from '../../../model/core/httpRequest';


export interface ICourseData {
    code: string,
    description:string,
    imageFileName:string,
    isActive:boolean,
    isFree:boolean,
    name: string,
    languageId: string,
    minRank: number,
    maxRank: number
}

export interface ICourseResponse {
    status: boolean,
    data: Array<ICourseData>,
    message: string
}

@Injectable()
export class CourseService {
    constructor(private http: Http) { }

    getSentencePack(languageId: string, level: string, category: string) {
        let params = `languageId=${languageId}&level=${level}&category=${category}`;
        let req = {
            method: RequestMethod.Get,
            url: '/api/sentence',
            search: params
        };

        return httpRequest(this.http, req);
    }

    showRelatedWordsInfo(languageId: string, text: string) {
        let params = `languageId=${languageId}&text=${text}`;
        let req = {
            method: RequestMethod.Get,
            url: '/api/word/info',
            search: params
        };

        return httpRequest(this.http, req);

    }

    buildCourse(languageId: string) {
        let req = {
            url: `/api/course/build`,
            method: RequestMethod.Get,
            search: `languageId=${languageId}`
        };

        return httpRequest(this.http, req);
    }
    getUserCourse(languageId: string) {
        let req = {
            url: `/api/courses/user`,
            method: RequestMethod.Get,
            search: `languageId=${languageId}`
        };
        return httpRequest(this.http, req);
    }
    getCourse(languageId: string) {
        let req = {
            url: `/api/courses`,
            method: RequestMethod.Get,
            search: `languageId=${languageId}`
        };
        return httpRequest(this.http, req);
    }
    getCourseData(CourseCode: string) {
        let req = {
            url: `/api/course`,
            method: RequestMethod.Get,
            search: `code=${CourseCode}`
        };
        return httpRequest(this.http, req);
    }

    getCourseStatistics(languageId: string) {
        let req = {
            url: `/api/course/statistics`,
            method: RequestMethod.Get,
            search: `languageId=${languageId}`
        };

        return httpRequest(this.http, req);
    }

    getCourseNextModule(code: string) {
        let req = {
            url: `/api/course/module`,
            method: RequestMethod.Get,
            search: `code=${code}`
        };

        return httpRequest(this.http, req);
    }

    startCourse(CourseCode: string) {
        let req = {
            url: `/api/course/start`,
            method: RequestMethod.Get,
            search: `code=${CourseCode}`
        };
        return httpRequest(this.http, req);
    }

    updateSentenceShowKnowledge(mode:string,sentence:any){
     let req = {
            url: ` /api/knowledge/update/sentence`,
            method: RequestMethod.Get,
            search: `mode=${mode}&languageId=${sentence.languageId}&sentenceText=${sentence.text}`
        };
        return httpRequest(this.http, req);
    }

    updateWordClickedKnowledgeText(type:string,mode:string,languageId:string,attempt:number,word:any){    
        let lemma = '';
        if(word.lemma !== undefined && word.lemma != ''){
          lemma =  `&lemma=${word.lemma}`; 
        }    

         let attemptCount ='';
        if(attempt != 0)
        {
            attemptCount = `&attempt=${attempt}`
        }

         let req = {
            url: `/api/knowledge/update/word`,
            method: RequestMethod.Get,
            search: `type=${type}&mode=${mode}&languageId=${languageId}&text=${word.text}${lemma}${attemptCount}`
        };
        return httpRequest(this.http, req);
    }

    getCourseProgress(code:string){
        let req = {
            url: `/api/courses/user/progress`,
            method: RequestMethod.Get,
            search: `code=${code}`
        };
        return httpRequest(this.http, req);
    }

    
}

@NgModule({
    providers: [
        CourseService
    ]
})

export default class providerCourseService { }