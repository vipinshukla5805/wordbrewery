import { Component, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Router, RouterModule } from "@angular/router";

import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import CommonFooterModule from '../../../comp/footer/common-footer';
import DropdownModule, { DropdownComponent, IDropdownOption } from '../../../comp/dropdown/dropdown';
import * as common from '../../../model/core/common';

import { ListSettingService } from '../../../model/settings/listSetting.service';
import { ClientLanguage, RTL_LANGUAGES } from  '../../../model/settings/clientLanguage.class';
import { ClientSettings } from '../../../model/settings/clientSettings.class';

import providerCourseService, { CourseService, ICourseResponse, ICourseData } from '../model/course.service';


@Component({
  templateUrl: '/app/routes/courses/course-home/course-home.html'
})



export class CourseHomeComponent {

  private _subs: Array<Subscription> = [];
  private selectedLanguage: string;
  private ddlLevels: Array<IDropdownOption> = [];
  private ddlLanguages: Array<IDropdownOption> = [];
  private ddlSortingOptions: Array<IDropdownOption> = common.SORTING_OPTIONS;

  private courses: Array<ICourseData> = [];
  private isCourseModeLoading: boolean = false;
  private isCourseModeBuilding: boolean = false;

  private textChange$: Subject<string> = new Subject<string>();
  private cancelInterval: any;
  settings: ClientSettings = new ClientSettings();

  constructor(
    private router: Router,
    private listSettingService: ListSettingService,
    private courseService: CourseService
  ) {
  }

  ngOnInit() {
    this._subs.push(
      this.listSettingService.listSettings$.subscribe(res => {
        if (!res) return;

        this.settings = res.settings;
        this.ddlLanguages = res.listSettings.languages;
        this.ddlLevels = res.listSettings.levels;

        let languages = this.ddlLanguages.filter(res => res.selected);
        languages[0].value ? this.selectedLanguage = languages[0].value : this.selectedLanguage = 'English';
      })
    );

    this._subs.push(
      this.textChange$.debounceTime(550).subscribe()
    );

    this.cancelInterval = setInterval(this.loadCourses(this.selectedLanguage), 1000);
  }


  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe();
  }


  private onChangeLanguage(lang: ClientLanguage) {
    this.selectedLanguage = lang.languageId;
    this.settings.currentLanguage = lang;
    this.loadCourses(lang.languageId);
  }

  private onLevelChange(data) {

  }

  private onSortingChange(data) {

  }

  private loadCourses(languageId: string = 'English') {
    this.isCourseModeLoading = true;

    this.courseService.getCourse(languageId)
      .subscribe((res: ICourseResponse) => {
        if (res.status == true && res.data) {
          this.courses = res.data;
        }
        this.isCourseModeLoading = false
      }, () => this.isCourseModeLoading = false);
  }

  private selectCourse(courseCode: string) {
    this.router.navigateByUrl(`/course/study/${courseCode}`);
    // this.router.navigate(['/course/study', courseCode]);     
  }
}

@NgModule({
  declarations: [
    CourseHomeComponent
  ],
  imports: [
    DropdownModule,
    RouterModule,
    CommonModule,
    CommonFooterModule
  ],
  providers: [
    CourseService
  ]
})
export class CourseHomeModule { }
