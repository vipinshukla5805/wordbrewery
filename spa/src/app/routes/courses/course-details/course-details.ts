import { Component, NgModule, HostListener } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ActivatedRoute, Router, RouterModule } from "@angular/router";
import {
  FormGroup, AbstractControl,
  Validators, FormBuilder,
  ReactiveFormsModule, FormsModule
} from '@angular/forms';

import { ProgressbarModule } from 'ng2-bootstrap/ng2-bootstrap';

import CommonFooterModule from '../../../comp/footer/common-footer';
import { WordInfoBoxModule } from '../../../comp/word-info-box/word-info-box';
import { SentenceInfoBoxModule } from '../../../comp/sentence-info-box';

import providerCourseService, { CourseService, ICourseResponse, ICourseData } from '../model/course.service';

import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription'
import { Observable } from 'rxjs/Observable'
import { Subject } from 'rxjs/Subject';

import { CarouselModule } from 'ng2-bootstrap/ng2-bootstrap';

import { ClientSentence } from '../../../model/clientSentence.class';
import { Word } from '../../../model/word.class';
import { RequestStatus } from '../../../model/requestStatus.class';
import { Popovers } from '../../../model/popovers/popovers';

import { ClientLanguage, RTL_LANGUAGES } from '../../../model/settings/clientLanguage.class';
import { ClientSettings } from '../../../model/settings/clientSettings.class';
import { SettingsService } from '../../../model/settings/settings.service';
import { AuthenticationService, IUserData, IUserStatisticData } from '../../../model/user/authentication.service';
import { ListSettingService } from '../../../model/settings/listSetting.service';

import { ReportService } from '../../../model/report/report.service'
import { AudioService } from '../../../model/core/audio'
import DropdownModule, { DropdownComponent, IDropdownOption } from '../../../comp/dropdown/dropdown'
import StatisticsModule from '../../../comp/statistics-counter'

export interface reviewWords {
  word: Word,
  isSelected: boolean
}

@Component({
  templateUrl: '/app/routes/courses/course-details/course-details.html'
})

export class CourseDetailComponent {
  private _subs: Array<Subscription> = [];
  private settings: ClientSettings = new ClientSettings();
  private sentences: ClientSentence[] = [];
  private selectedWords: Array<Word> = new Array<Word>();
  private reviewSelectedWords: Array<reviewWords> = new Array<reviewWords>();
  private isRTL: boolean = false;
  private wordDefinitions: any = [];
  private sentenceReported: any
  private user: IUserData;
  private requestStatus: RequestStatus = new RequestStatus();
  private mainRequestStatus: RequestStatus = new RequestStatus();
  private isSentenceSelectorVisible: boolean = false;
  private selectedWord: string = '';
  private selectedSentence: ClientSentence;
  private clickedWord: Word;
  private wordsShown: IUserStatisticData;
  private sentencesShown: IUserStatisticData;
  private isCourseCompleted: boolean;
  private isServiceFailed: boolean;
  private progressVal: number = 0;
  private courseCode: string;
  private reviewWordsCount:number = 0;
  private currentlanguage:string;
  private canContinue:boolean = false;

  private textChange$: Subject<any> = new Subject<any>();

  constructor(private route: ActivatedRoute,
    private router: Router,
    private courseService: CourseService,
    private settingsService: SettingsService,
    private popovers: Popovers,
    private userService: AuthenticationService,
    private audio: AudioService,
    private listSetting: ListSettingService,
    private _fb: FormBuilder
  ) {
    this.selectedWords = [];
    this.isCourseCompleted = false;
    this.isServiceFailed = false;
  }

  @HostListener('click', [ '$event' ])
  onClick(event: any): any {
    this.popovers.processDocumentOnClick();
  }

  ngOnInit() {
    let courseCode;
    this.route.params.forEach((params: any) => {
      courseCode = params[ 'code' ]; // (+) converts string 'id' to a number
      this.courseCode = params[ 'code' ];
    });
    this.loadSettings();
    this.loadUser();
    this.load(courseCode);
  }

  loadSettings() {
    this._subs.push(
      this.settingsService.loadSettings().subscribe(res => {
        if (res.settings)
          this.settings = res.settings
      })
    );
  }
  currentSentenceIndex: number = 0;
  currentWords: Array<Word> = new Array<Word>();

  load(courseCode) {
    this.isRTL = RTL_LANGUAGES.indexOf(this.settings.currentLanguage.languageId) > -1;
    this.mainRequestStatus.isServerRequesting = true;
    this._subs.push(
      this.courseService.getCourseNextModule(courseCode)
        .subscribe(data => {
          //this.popovers.hide();
          if (data.data != null) {
            this.sentences = data.data.sentences;
            this.isCourseCompleted = data.isCourseFinished;
            if (this.sentences.length > 0 && !this.isCourseCompleted) {
              this.currentWords = this.sentences[ 0 ].words;
              this.mainRequestStatus.isServerRequesting = false;
              this.selectedSentence = this.sentences[ 0 ];
              //To do --If setences not available from api this may break
              this.currentlanguage =  this.selectedSentence.languageId;
              this.updateSentenceShowKnowledge(this.sentences[ 0 ]);
              this.canContinue =  true;
              sessionStorage.setItem("currentCourseModule", JSON.stringify(data.data));
              this.calculateProgress();
            } else if (this.sentences.length == 0) {
              this.isServiceFailed = true;
              this.canContinue =  false;
            } else if(this.isCourseCompleted) {
              this.isCourseCompleted = true
            }
          }
        },
        err => { this.mainRequestStatus.isServerRequesting = false;this.canContinue =  false; },
        () => { this.mainRequestStatus.isServerRequesting = false; }
        ));
  }

  onWordClicked(data: any): any {
    let { word, sentence } = data;
    // insert only if this word does not exist
    let doesWordExist = this.selectedWords.findIndex((item: Word) => (
      item.lemma == word.lemma || item.text == word.text
    )) > -1;

    if (!doesWordExist) {
      this.reviewSelectedWords.push({ word: word, isSelected: false });
      this.selectedWords.push(word);
    }

    this.cancelSubscription();

    // highlighting word
    word.isHighlighted = true;
    if (word.isPunctuation) return;

    this.updateWordClickedKnowledge(word);
  }

  loadUser(): any {
    //this.userService.getUser();
    this.userService.userData$.subscribe(res => {
      this.user = res
    });
  }

  private reviewWords(word: Word) {

    this.reviewSelectedWords.filter(val => {
      if (val.word == word && val.isSelected == false) {
        ++this.reviewWordsCount;
        return val.isSelected = true;
      } else if (val.word == word && val.isSelected == true) {
        --this.reviewWordsCount;
        return val.isSelected = false;
      }
    });
    console.log(this.reviewSelectedWords);
  }

  private cancelSubscription() {
    while (this._subs.length) {
      this._subs.pop().unsubscribe()
    }
  }

  private getNextSentence() {
    if (this.sentences.length - 1 == this.currentSentenceIndex) {
      this.router.navigateByUrl(`/course/quiz/${this.courseCode}/${this.progressVal}`);
      return;
    }
    ++this.currentSentenceIndex;
    this.currentWords = this.sentences[ this.currentSentenceIndex ].words;
    //Close Model
    $('#words-modal').hide();
    this.resetWordInfoBox();
    this.updateSentenceShowKnowledge(this.sentences[ this.currentSentenceIndex ]);
    this.selectedSentence = this.sentences[ this.currentSentenceIndex ];
    this.reviewWordsCount=0;

  }

  private getPrevSentence() {
    --this.currentSentenceIndex;
    if (this.currentSentenceIndex >= 0)
      this.currentWords = this.sentences[ this.currentSentenceIndex ].words;
    this.selectedSentence = this.sentences[ this.currentSentenceIndex ];
  }

  private resetWordInfoBox() {
    this.reviewSelectedWords = new Array<reviewWords>();
    this.selectedWords = new Array<Word>();
  }

  private calculateProgress() {
    this.courseService.getCourseProgress(this.courseCode)
      .subscribe(res => this.progressVal = res.data)
  }

  private updateSentenceShowKnowledge(sentence: ClientSentence) {
    this.courseService.updateSentenceShowKnowledge('COURSE',sentence).subscribe();
  }

  private updateWordClickedKnowledge(word: Word) {
    this.courseService.updateWordClickedKnowledgeText('CLICKED','COURSE',this.currentlanguage,0,word).subscribe();
  }
}

@NgModule({
  declarations: [
    CourseDetailComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    CommonFooterModule,
    FormsModule,
    ReactiveFormsModule,
    ProgressbarModule,
    CarouselModule,
    WordInfoBoxModule,
    SentenceInfoBoxModule
  ],
  providers: [
    CourseService
  ]
})
export class CourseDetailModule { }
