import { Component, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ActivatedRoute, Router, RouterModule } from "@angular/router";

import { Subscription } from 'rxjs/Subscription';

import { ClientSentence } from  '../../../model/clientSentence.class';
import { Word } from '../../../model/word.class';

import CommonFooterModule from '../../../comp/footer/common-footer';
import DropdownModule, { DropdownComponent, IDropdownOption } from '../../../comp/dropdown/dropdown';
import { ProgressbarModule } from 'ng2-bootstrap/ng2-bootstrap';

import providerCourseService, { CourseService, ICourseResponse, ICourseData } from '../model/course.service';

export interface IQuize {
  term: any,
  sentence: any,
  choices: any
}

export interface Ichoices{
  lemma?:string,
  pos?:string,
  text?:string
}

export interface IOption{
  isselected?:boolean,
  iscorrect?:boolean,
  isdisabled?:boolean,
  class?:string,
  disbaleclass:string,
  choice?: Ichoices
}


@Component({
  templateUrl: '/app/routes/courses/course-quiz/course-quiz.html'
})


export class CourseQuizComponent {
  private _subs: Array<Subscription> = [];
  private CourseData: ICourseData;
  private module: any;
  private quizQuestions: Array<IQuize> = new Array<IQuize>();
  private currentIndex: number = 0;
  private currentQuize: IQuize;
  private option1: string;
  private option2: string;
  private option3: string;
  private option4: string;
  private currentSentence: Array<Word> = new Array<Word>();
  private courseCode:any;
  private progressVal:number = 0; 
  private choices:Array<Ichoices>=new Array<Ichoices>();
  private Option:IOption;
  private Options:Array<IOption> =new Array<IOption>();
  private Answer:string;
  private skip:boolean = true;
  private inCorrectAnswer:boolean = false;
  private currentlanguage:string;
  private canContinue:boolean = false;
  private attempt:number = 1;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private courseService: CourseService
  ) {
    //Load quize questions from sessionStorage    
   
  }

  ngOnInit() {
    let courseCode;
    this.route.params.forEach((params: any) => {
      courseCode = params['code']; // (+) converts string 'id' to a number
      this.courseCode =  params['code'];
      this.progressVal = params['progress'];
    });
    this.module = JSON.parse(sessionStorage.getItem("currentCourseModule"));
    if(this.module) this.canContinue = true;
    this.prepareQuize(this.module);
    this.quizQuestions = this.module.quiz;
    this.currentQuize = this.quizQuestions[this.currentIndex];
    this.currentlanguage = this.currentQuize.sentence.languageId;
    this.prepareSentence();
    this.choices = this.currentQuize.choices;
    this.option1 = this.currentQuize.choices[0].text;
    this.option2 = this.currentQuize.choices[1].text;
    this.option3 = this.currentQuize.choices[2].text;
    this.option4 = this.currentQuize.choices[3].text;
    this.prepareOptions(this.choices);
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe();
  }


  private getNextQestion() {   

    if (this.quizQuestions.length-1 == this.currentIndex) {
      if(this.module.isCourseFinished){
        this.router.navigateByUrl(`/course`);
      }else{
        this.router.navigateByUrl(`/course/details/${this.courseCode}`);
      }
      return;
    }
    ++this.currentIndex;
    if (this.quizQuestions.length > this.currentIndex) {
      this.currentQuize = this.quizQuestions[this.currentIndex];
      this.prepareSentence();
      this.Options =new Array<IOption>();
      this.prepareOptions(this.currentQuize.choices);
      this.attempt = 1;
    }

    

  }

  private prepareQuize(module: any) {
    if (module !== undefined && module != null)
      this.quizQuestions = module.quiz;
  }
  private prepareSentence() {
    this.currentSentence = new Array<Word>();
    this.currentQuize = this.quizQuestions[this.currentIndex];  
    this.choices = this.currentQuize.choices;
    this.option1 = this.currentQuize.choices[0].text;
    this.option2 = this.currentQuize.choices[1].text;
    this.option3 = this.currentQuize.choices[2].text;
    this.option4 = this.currentQuize.choices[3].text;
    this.currentQuize.sentence.words.forEach(element => {

      if (this.currentQuize.term.text == element.text.toLowerCase()) {
        console.log(this.currentQuize.term.text);
        element.text = "______";
        this.currentSentence.push(element);
        this.Answer = this.currentQuize.term.text;
      }
      else{        
        this.currentSentence.push(element);
      }
        
    });
  }

  private prepareOptions(choices:Array<Ichoices>){
    
    debugger
    
    this.choices.forEach(val=>{
      
      this.Option =<IOption>{
       iscorrect:false,
       class:'',
       choice:null,
       isdisabled:false,
       isselected:false,
       disbaleclass:''
     };

      this.Option.iscorrect = false;
      this.Option.isdisabled =false;
      this.Option.isselected = false;
      this.Option.class ="";
      this.Option.choice =val;
      this.Options.push(this.Option);

      console.log( !this.Option.iscorrect && this.Option.isdisabled);
    
    });

     console.log("Options"+ this.Options);
    
  }
  
  private checkAnswer(word:string){

   this.Options.forEach(val =>{
     if(this.Answer == word && val.choice.text == word){       
       val.iscorrect =true;
       val.isdisabled =false;
       val.isselected = true;
       val.class ="quiz-btn-success";       
       this.skip = false;
       this.courseService.updateWordClickedKnowledgeText('CHOICE','QUIZ',this.currentlanguage,this.attempt,val.choice).subscribe();
       this.courseService.updateWordClickedKnowledgeText('TARGET','QUIZ',this.currentlanguage,this.attempt,val.choice).subscribe();
       this.inCorrectAnswer = false;
       ++this.attempt;
     }else if(this.Answer != word && val.choice.text == word){
       val.iscorrect =false;
       val.isdisabled =true;
       val.isselected = true;
       val.class ="quiz-btn-danger";       
       this.skip = true;
       this.courseService.updateWordClickedKnowledgeText('CHOICE','QUIZ',this.currentlanguage,this.attempt,val.choice).subscribe();
       this.courseService.updateWordClickedKnowledgeText('TARGET','QUIZ',this.currentlanguage,this.attempt,val.choice).subscribe();
       this.inCorrectAnswer = true;
       ++this.attempt;
     }
     else{
       val.iscorrect =false;
       val.isdisabled == true ? val.isdisabled = true : val.isdisabled = false;  
       val.iscorrect == true ? val.iscorrect = true : val.iscorrect = false; 
       val.isselected = false;
       val.class =""
     }
     !val.iscorrect && val.isdisabled ? val.disbaleclass = 'disabled' : val.disbaleclass='';     
     console.log(val.disbaleclass);
   })

   
    
  }
}

@NgModule({
  declarations: [
    CourseQuizComponent
  ],
  imports: [
    DropdownModule,
    RouterModule,
    CommonModule,
    CommonFooterModule,
    ProgressbarModule
  ],
  providers: [
    CourseService
  ]
})
export class CourseQuizModule { }