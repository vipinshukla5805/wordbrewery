// import { Component } from '@angular/core'
// import {
//     RouteConfig, RouterOutlet, RouterLink,
//     ROUTER_DIRECTIVES, Router, CanActivate,
//     ComponentInstruction
// } from "@angular/router-deprecated";

// import {
//     CourseDashboard,
//     CourseDetail,
//     CourseStudy,
//     CourseHome,
//     providerCourseService
// } from './index'

// import { isUserLoggedIn } from '../../model/user/authentication.service'

// @CanActivate((next: ComponentInstruction, prev: ComponentInstruction) => isUserLoggedIn(false, ['/Account', 'Login']))
// @Component({
//     template: `<router-outlet></router-outlet>`,
//     directives: [ROUTER_DIRECTIVES],
//     providers: [ providerCourseService ]
// })

// @RouteConfig([
//     {
//         path: '/details',
//         component: CourseDetail,
//         name: 'CourseDetail'
//     },
//     {
//         path: '/study',
//         component: CourseStudy,
//         name: 'CourseStudy'
//     },
//     {
//         path: '/dashboard',
//         component: CourseDashboard,
//         name: 'CourseDashboard'
//     },
//     {
//         path: '/',
//         component: CourseHome,
//         name: 'CourseHome'
//     }
// ])

// export class Course {
//     constructor(private router: Router) { }

//     isActive(instruction: any[]): boolean {
//         return this.router.isRouteActive(this.router.generate(instruction));
//     }
// }


import { Component, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import CommonFooterModule from '../../comp/footer/common-footer';

@Component({
    template: `<router-outlet></router-outlet>`
})

export class CourseComponent {}

@NgModule({
  declarations: [
    CourseComponent
  ],
  imports: [    
    RouterModule,
    CommonModule,
    CommonFooterModule
  ]
})
export  class CourseModule { }