import { Component, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ActivatedRoute, Router, RouterModule } from "@angular/router";

import { Subscription } from 'rxjs/Subscription';

import CommonFooterModule from '../../../comp/footer/common-footer';
import DropdownModule, { DropdownComponent, IDropdownOption } from '../../../comp/dropdown/dropdown';

import providerCourseService, { CourseService, ICourseResponse, ICourseData } from '../model/course.service';

import { RequestStatus } from '../../../model/requestStatus.class';
import { SettingsService } from '../../../model/settings/settings.service';

@Component({
  templateUrl: '/app/routes/courses/course-study/course-study.html'
})

export class CourseStudyComponent {
  private _subs: Array<Subscription> = [];
  private CourseData: ICourseData;
  private courseCode: string;
  private courseLanguageId: string;
  private courseStatus: string = 'Start';
  private requestStatus = new RequestStatus();
  private IsCourseStarted: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private courseService: CourseService,
    private settingService: SettingsService
  ) {
  }

  ngOnInit() {
    this.requestStatus.isServerRequesting = true;
    this._subs.push(
      this.route.params.subscribe((param: any) => {
        this.courseCode = param['code'];
        if (!this.courseCode) return;

        this._subs.push(
          this.courseService.getCourseData(this.courseCode)
            .subscribe((res: any) => {
              if (res.status == true && res.data) {
                this.CourseData = res.data;
                this.courseLanguageId = this.CourseData.languageId;
                this._subs.push(
                  this.courseService.getUserCourse(this.courseLanguageId)
                    .subscribe(res => {                      
                      if (res.status == true && res.data[0]) {

                        if (this.courseCode == res.data[0].course.code) {
                          this.courseStatus = 'Continue';
                          this.requestStatus.isServerRequesting = false;
                        }
                      }
                    }, err => { this.requestStatus.isServerRequesting = false }
                    , () => this.requestStatus.isServerRequesting = false)
                );
              }
            }));
      }));
  }

  ngOnDestroy() {
    while (this._subs.length) this._subs.pop().unsubscribe();
  }

  private onStart(courseCode: string) {
    if (this.courseStatus == 'Continue') {
      this.router.navigateByUrl('/course/details/' + courseCode);
    }
    else {
      //To do once --> Get the list of user's courses apis available
      this._subs.push(
        this.courseService.startCourse(courseCode)
          .subscribe((res: ICourseResponse) => {
            if (res.status == true) {
              this.router.navigateByUrl('/course/details/' + courseCode);
            }
          }));
    }

  }
}

@NgModule({
  declarations: [
    CourseStudyComponent
  ],
  imports: [
    DropdownModule,
    RouterModule,
    CommonModule,
    CommonFooterModule
  ],
  providers: [
    CourseService
  ]
})
export class CourseStudyModule { }