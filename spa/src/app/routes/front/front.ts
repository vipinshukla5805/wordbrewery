import { Component, OnInit, NgModule } from "@angular/core";
import { RouterModule, Router } from "@angular/router";
import { CommonModule } from "@angular/common";

import { Subscription } from 'rxjs/Subscription'

import { FrontPageService, IFeedData } from './model/front.service'
import { RequestStatus } from '../../model/requestStatus.class';

import { FooterComponent } from '../../comp/footer/footer'

@Component({
  templateUrl: '/app/routes/front/front.html'
})

export class FrontPageComponent {
  private feedList: Array<IFeedData> = [];
  private requestStatus = new RequestStatus();
  private _subs: Array<Subscription> = [];

  constructor(
    private frontSvc: FrontPageService,
    private router: Router
  ) {
  }

  getFeedImage(content: string) {
    let rex = /<img[^>]+src="([^">]+)/g
    let urls =  rex.exec(content);
    return `${urls[1]}`;
  }

  ngOnInit() {
    this.requestStatus = {
      isServerRequesting : true,
      isError: false,
      isSuccess: true,
    }

    this._subs.push(
    this.frontSvc.getFeed()
    .subscribe(res => {
      this.requestStatus.isSuccess = true;
      this.feedList = res;
    }, () => this.requestStatus.isError = true, () => this.requestStatus.isServerRequesting = false));
  }

  ngOnDestroy() {
    while(this._subs.length > 0) {
      this._subs.pop().unsubscribe();
    }
  }

  private navigateToExplore() { 
    this.router.navigateByUrl('/explore');
  }
}

@NgModule({
  imports: [
    RouterModule,
    CommonModule
  ],
  declarations : [
    FrontPageComponent,
    FooterComponent
  ],
  providers : [
    FrontPageService
  ]
})
export default class FrontPageModule {}
