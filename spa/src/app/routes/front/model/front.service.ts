import
{
    Injectable, NgModule
} from '@angular/core';
import
{
    Http, RequestMethod, Headers, Jsonp, URLSearchParams
} from '@angular/http';

import { httpRequest } from '../../../model/core/httpRequest'
import { SettingsService } from '../../../model/settings/settings.service';

export interface IFeedData {
    title: string;
    contentSnippet: string;
    publishedDate: string;
    categories: Array<string>;
    author: string;
}

@Injectable()
export class FrontPageService {

    constructor(
        private jsonp: Jsonp,
        private http: Http,
        private settingSvc: SettingsService
    ) {
    }

    getFeed() {
        let feedUrl = `https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=3&callback=JSONP_CALLBACK&q=${encodeURIComponent('http://feeds.feedburner.com/LanguageUntapped')}`;
        return this.jsonp
            .get(feedUrl)
            .map((res: any) => {
                let formattedResponse = res.json();
                return formattedResponse ? formattedResponse.responseData.feed.entries : [];
            });
    }
}