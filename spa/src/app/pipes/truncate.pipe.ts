import { NgModule, Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'truncate'})
export class Truncate {
  transform(value: string) : string {
    if(!value) return value;
    
    return value.replace(/^(.+)@(.+)$/g,'$1');
  }
}