import {Injectable} from 			'@angular/core';
import {Http, Headers} from 		'@angular/http';

@Injectable()
export class SentenceQuizService {

	public headers: Headers = new Headers();

    constructor(public http: Http) {}
// /api/squiz?type=user&listId={listId}&number={number}
	public getSentenceListQuiz(type: any , listId: any , numb: any): any {
		this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');

		var endpoint = '/api/squiz';
		var params =  '?type=' + encodeURIComponent(type) +
                       '&listId=' + encodeURIComponent(listId) +
                       '&number=' + numb;
                       console.log(endpoint + params);
		return this.http.get(endpoint + params, { headers: this.headers })
            .map(res => res.json());
	}
}
