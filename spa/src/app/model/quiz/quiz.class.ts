import {QuizData} from 				'./quizData.class';
export class Quiz {

    public languageId: string = '';
    public listName: string = '';
    public level: string = '';
    public quizDataList: QuizData[] = [];

}
