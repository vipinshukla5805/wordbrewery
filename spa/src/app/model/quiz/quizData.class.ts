import {Word} from 				'../word.class';
import {ClientSentence} from 	'../clientSentence.class';
export class QuizData {

    public word: Word = new Word();
    public sentence: ClientSentence = new ClientSentence();
    public choices: Word[] = [];

}
