import {Injectable} from 			'@angular/core';
import {Http, Headers} from 		'@angular/http';

@Injectable()
export class WordQuizService {

	public headers: Headers = new Headers();

    constructor(public http: Http) {}

	public getWordListQuiz(type: any , languageId: any , levelId: any , listId: any , numb: any): any {
		this.headers.set('Accept', 'application/json');
        this.headers.set('Content-Type', 'application/json');

		var endpoint = '/api/quiz';
		var params =  '?type=' + encodeURIComponent(type) +
                       '&languageId=' + encodeURIComponent(languageId) +
                       '&levelId=' + encodeURIComponent(levelId) +
                       '&listId=' + encodeURIComponent(listId) +
                       '&number=' + numb;
                       console.log(endpoint + params);
		return this.http.get(endpoint + params, { headers: this.headers })
            .map(res => res.json());
	}
    public getWordListQuizSpecial(specName: any, numb: any): any
    {
        this.headers.set('Accept' , 'application/json');
        this.headers.set('Content-Type' , 'application/json');
        var endpoint = '/api/quiz';
        var params = '?type=specialized' +
                     '&languageId=English' +
                     '&listName=' + specName +
                     '&number=' + numb;
        return this.http.get(endpoint + params, {headers: this.headers})
            .map(res => res.json());
    }
    public getWordListQuizCurrentLevel(languageId: any , level: any , number: any): any
    {
        this.headers.set('Accept' , 'application/json');
        this.headers.set('Content-Type' , 'application/json');
        var endpoint = '/api/quiz';
        var params = '?type=primary' +
                     '&languageId=' + languageId +
                     '&levelId=' + level +
                     '&number=' + number;
        return this.http.get(endpoint + params, {headers: this.headers})
            .map(res => res.json());
    }
}
