export class Source {
    public url: 			string;
    public language: 		string;
    public category: 		string;
    public siteName: 		string;
    public linkSelector: 	string;
    public textSelector: 	string;
    public date:		 	string;
}
