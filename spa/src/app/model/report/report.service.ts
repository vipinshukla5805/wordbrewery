import 'rxjs/add/operator/share'
import
{
  Injectable, NgModule
} from '@angular/core';
import
{
  Http, RequestMethod, Headers, Response, Request
} from '@angular/http';
import
{
  httpRequest
} from '../core/httpRequest';

@Injectable()
export class ReportService {
  constructor(private http: Http) { }

  report(sentenceData) {
    let req = {
      method: RequestMethod.Get,
      url: '/sentence/report.json',
      body: JSON.stringify(sentenceData),
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      })
    };
    return this.http.post('/sentences/report.json', JSON.stringify(sentenceData), { headers: req.headers })
      .map((res: Response) => {
        return res.json()
      }).share();
  }
}
