import { KnowledgeData } from './knowledge/knowledgeData.class';

export interface IDef {
    glosses : Array<string>;
    token   : string;
}

export class Word {
    public languageId: string;
    public text: string;
    public pos: string;
    public posMeaning: string;
    public lemma: string;
    public synsetId: string;
    public ne: string;
    public frequencyPosition: number;
    public isSpaceAfter: boolean;
    public isUnlisted: boolean;
    public isPunctuation: boolean;
    public punctuationType: string;
    public isInitQuotePunctuation: boolean;
    public isAddSpaces: boolean;
    public isHighlighted: boolean;
    public knowledgeData: KnowledgeData;
    public definitions: string[];
    public defs: Array<IDef>;
    public translation: string = '';
    public relatedWords: any = null;

    public formatWord(newWord: Word): any {
    	var format = '';
    	if (newWord.pos) {
    		format = format + '<p class=\'wb-pos\'>POS: ' + newWord.pos + '</p>';
    	}
    	if (newWord.lemma) {
    		format = format + '<p class=\'wb-lemma\'>Lemma: ' + newWord.lemma + '</p>';
    	}
    	if (newWord.isUnlisted) {
    		format = format + '<p class=\'wb-unlistes\'>Unlisted</p>';
    	} else {
    		format = format + '<p class=\'wb-frequency-position\'>Frequency rank: ' + newWord.frequencyPosition + '</p>';
    	}
		return format;
    }
}
