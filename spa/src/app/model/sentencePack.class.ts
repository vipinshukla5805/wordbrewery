export class SentencePack {
    public language: 	string;
    public level: 		string;
    public category:	string;
    public sentences: 	string[];
}
