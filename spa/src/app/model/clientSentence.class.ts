import { Word } from './word.class';
import { Translation } from './translator/translation.class';

export class ClientSentence {
    public languageId:        string;
    public level: string;
    public category: string;
    public text: string;
    public date: string;
    public source: string;
    public siteName: string;
    public score: number;
    public words: Word[];
    public unlistedWords: number;
    public translations: Translation[];
    public ignorance: number;
}
