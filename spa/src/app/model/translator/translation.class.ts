export class Translation {
    public sentence: string;
    public translation: string;
    public source: string;
    public from: string;
    public to: string;
}
