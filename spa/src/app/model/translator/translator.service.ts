import {Injectable, NgModule} from  '@angular/core';
import { Headers, Http, RequestMethod } from '@angular/http';
import { Popovers } from  '../popovers/popovers';
import { httpRequest } from '../core/httpRequest'

const ACCESS_TOKEN = 'wb_tts_access-token';

@Injectable()
export class TranslatorService {
  public supportedLanguages: Array<string>;
  public isTranslating: boolean = false;

  constructor(
    private popovers: Popovers,
    private http: Http
  ) {
  }

  public getSupportedLanguageForSpeak(): void {
    this.supportedLanguages = localStorage.getItem(ACCESS_TOKEN)
      ? JSON.parse(localStorage.getItem(ACCESS_TOKEN)) : null;
    if (!this.supportedLanguages) {
      let req = {
        method: RequestMethod.Get,
        url: '/api/tts/languages',
      };
      httpRequest(this.http, req)
        .subscribe(data => {
          this.supportedLanguages = data;
          localStorage.setItem(ACCESS_TOKEN, JSON.stringify(data));
        })
    }
  }

  public getTranslation(text: string, from: string, to: string = 'English'): any {
    let params = `from=${from}
										&to=${to}
										&text=${text}`;
    let req = {
      method: RequestMethod.Get,
      url: '/api/word/translate',
      search: params
    };

    return httpRequest(this.http, req);
  }

  public getSentenceTranslation(text: string, from: string, to: string = 'English', provider: string = 'microsoft'): any {
    let params = `from=${from}&to=${to}&provider=${provider}&text=${text}`;
    let req = {
      method: RequestMethod.Get,
      url: '/api/sentence/translate',
      search: params
    };

    return httpRequest(this.http, req);
  }

  public getSentenceTranslations(text: string, from: string, to: string = 'English'): any {
    let params = `from=${from}&to=${to}&text=${text}`;
    let req = {
      method: RequestMethod.Get,
      url: '/api/sentence/translations',
      search: params
    };

    return httpRequest(this.http, req);
  }

  // text : text to translate
  // from : language code ("en" etc)
  // title: popover title where the translation will be shown
  // target: popover target element (event.target)
  public translate(text: string, from: string, title: string, target: HTMLElement): any {
    if (this.isTranslating) {
      return;
    }

    this.popovers.hide();
    this.isTranslating = true;

    var to = 'en';
    this.getTranslation(text, from, to)
      .subscribe(data => {
        target.setAttribute('data-content', data);
        $(target).popover('show');
      },
      null,
      () => {
        this.isTranslating = false;
      });
  }
}
