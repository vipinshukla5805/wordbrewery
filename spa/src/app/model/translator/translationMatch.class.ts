export class TranslationMatch {
    public error:				string;
    public MatchDegree:			number;
    public MatchedOriginalText: string;
    public Rating: 				number;
    public Count: 				number;
    public TranslatedText: 		string;
}
