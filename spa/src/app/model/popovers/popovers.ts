import {Injectable, NgModule} from '@angular/core';

@Injectable()
export class Popovers {
  public init(): any {
    $('[data-toggle-popover=\'popover\']').popover({
    		trigger: 'manual',
    		container: 'body',
    		html: true
    }).click(function(e) {
      e.preventDefault();
    });
  }

  public hide(destroy: boolean = false): any {
    $('[data-toggle-popover=\'popover\'], .popover').popover(destroy ? 'destroy' : 'hide');
  }

  public fade(): any {
    setTimeout(() => {
      $('.popover').fadeOut(1000);
    }, 1000);
  }

  public processDocumentOnClick(): any {
    this.hide();
  }
}
