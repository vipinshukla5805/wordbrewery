export class RequestStatus {
  public isServerRequesting: boolean = false;
  public isError: boolean = false;
  public isSuccess: boolean = false;

  constructor(data: any = null) {
    if (data)
      Object.assign(this, data);
  }
}
