export class KnowledgeData {

    public date: string;
    public numberShown: number;
    public knowledgeLevel: number;

}
