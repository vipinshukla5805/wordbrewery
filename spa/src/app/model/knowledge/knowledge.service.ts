import { Injectable, NgModule } from 			'@angular/core';
import { Http, Headers, RequestMethod } from 		'@angular/http';

import {Word} from                  '../word.class';

import { httpRequest } from '../core/httpRequest'

@Injectable()
export class KnowledgeService {

  public headers: Headers = new Headers();

  constructor(private http: Http) { }

  public getWordKnowledge(languageId: any, word: any): any {
    let params = `languageId=${encodeURIComponent(languageId)}&word=${encodeURIComponent(word)}`;
    let req = {
      url: '/api/knowledge',
      mehtod: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  public updateWordKnowLedge(knowledge: any, word: Word): any {
    let params = `languageId=${encodeURIComponent(word.languageId)}&word=${encodeURIComponent(word.text)}`;
    let req = {
      url: '/api/knowledge',
      mehtod: RequestMethod.Put,
      search: params,
      body: JSON.stringify(knowledge)
    }

    return httpRequest(this.http, req)
  }

  public getWordKnowledgeLemmatizedLanguages(languageId: any, lemma: any, pos: any) {
    let params = `languageId=${encodeURIComponent(languageId)}&lemma=${encodeURIComponent(lemma)}&pos=${encodeURIComponent(pos)}`;
    let req = {
      url: '/api/knowledge',
      mehtod: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  public updateWordKnowledgeLemmatizedLanguages(languageId: any, word: Word) {
    let params = `languageId=${encodeURIComponent(languageId)}&lemma=${encodeURIComponent(word.lemma)}&pos=${encodeURIComponent(word.pos)}`;
    let req = {
      url: '/api/knowledge',
      mehtod: RequestMethod.Get,
      search: params,
      body: JSON.stringify(word.knowledgeData)
    }

    return httpRequest(this.http, req)  
  }
}
