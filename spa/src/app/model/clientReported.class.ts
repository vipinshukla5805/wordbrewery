import { ClientSentence } from './clientSentence.class';

export class ClientReportedSentence {
	public clientSentence: ClientSentence;
	public reason: string;
}
