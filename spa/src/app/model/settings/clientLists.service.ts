import { Injectable, NgModule } from 			'@angular/core';
import { Http, RequestMethod, Headers } from 		'@angular/http';
import { ClientSettings } from 		'./clientSettings.class';

import { httpRequest } from '../core/httpRequest'

@Injectable()
export class ClientListsService {

    constructor(private http: Http) {}

	public loadLists(settings: ClientSettings): any {
		let requestOptions = {
			url: '/settings/get/lists.json',
			method: RequestMethod.Post,
			body: JSON.stringify(settings)
		};

		return httpRequest(this.http, requestOptions);
	}
}
