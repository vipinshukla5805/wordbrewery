import { Injectable, NgModule } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ClientListsService } from '../../model/settings/clientLists.service';
import { SettingsService } from '../../model/settings/settings.service';

export const TOPICS_ICON_SCHEMS = {
  'Business and Money': 'wb-icon-building',
  'News and Politics': 'wb-icon-newspaper',
  'Culture, Arts, and Travel': 'web-icon-wine-glass',
  'Science, Tech, and Health': 'wb-icon-rocket',
  'Sports': 'wb-icon-person',
  'Anything': 'wb-icon-globe'
}

@Injectable()
export class ListSettingService {
  listSettings$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private settings;

  constructor(
    private clientListsService: ClientListsService,
    private settingsService: SettingsService
  ) {
  }

  loadDefaultSettings(forcedLoad: boolean = false): any {
    let hasValue = !!sessionStorage.getItem('settings');
    if (!hasValue || forcedLoad) {
      this.settingsService.loadSettings()
        .subscribe(data => {
          if (!data) return;

          this.settings = data.settings;
          this.loadLists();
        });
    }
    else {
      this.listSettings$.next(JSON.parse(sessionStorage.getItem('settings')));
    }
  }

  private loadLists(): any {
    this.clientListsService.loadLists(this.settings)
      .subscribe(data => {
        let languages = this.mapToLanguageData(data.lists.languages);
        let levels = this.mapToLevelData(data.lists.levels);
        let categories = this.mapToCategoryData(data.lists.categories);

        let settingData = {
          settings: this.settings,
          listSettings: {
            languages: languages,
            levels: levels,
            categories: categories
          }
        };
        sessionStorage.setItem('settings', JSON.stringify(settingData))
        this.listSettings$.next(settingData);
      });
  }

  private mapToLanguageData(arr) {
    return arr.map(data => {
      return {
        text: data.languageId,
        value: data.languageId,
        'class': 'flag-icon flag-icon-' + data.countryCode,
        selected: this.settings.currentLanguage.languageId == data.languageId,
        originalItem: data
      }
    })
  }

  private mapToLevelData(arr) {
    return arr.map(data => {
      return {
        text: data,
        value: data,
        'class': 'flag-icon ',
        selected: this.settings.currentLevel == data,
        originalItem: data
      }
    })
  }

  private mapToCategoryData(arr) {
    return arr.map(data => {
      return {
        text: data,
        value: data,
        'class': 'flag-icon ' + TOPICS_ICON_SCHEMS[ data ],
        selected: this.settings.currentCategory == data,
        originalItem: data
      }
    })
  }

}

export const LIST_SETTINGS_PROVIDERS = [
  { provide: ListSettingService, useClass: ListSettingService }
];