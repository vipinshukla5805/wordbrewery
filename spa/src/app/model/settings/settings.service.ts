import { Injectable, NgModule } from '@angular/core';
import { Http, RequestMethod, Jsonp } from '@angular/http';
import * as Cookies from 'js-cookie';
import { httpRequest } from '../core/httpRequest';

export const EU_COUNTRIES = [
	'Austria', 'Belgium', 'Bulgaria', 'Croatia', 'Cyprus', 'Czech Republic', 'Denmark', 'Estonia', 'Finland',
	'France', 'Germany', 'Greece', 'Hungary', 'Ireland', 'Italy', 'Latvia', 'Lithuania', 'Luxembourg', 'Malta',
	'Netherlands', 'Poland', 'Portugal', 'Romania', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'United Kingdom'
];

export interface IUserGeoLocation {
	countryCode : string;
	countryName : string;
	ipAddress   : string;
}

@Injectable()
export class SettingsService {

	constructor(private http: Http, private jsonp: Jsonp) { }

	public getUserGeoLocation() {
		let url = `//freegeoip.net/json/?callback=JSONP_CALLBACK`;
		return this.jsonp
            .get(url)
            .map(res => res.json());
	}

	public extractLanguage(): string {
		let ninjaSession = Cookies.get('NINJA_SESSION');

		let language = /language=(.*?)(&|$)/.exec(ninjaSession);
		if (language) {
			return language[1];
		} else {
			return '';
		}
	}

	public extractLevel(): string {
		let ninjaSession = Cookies.get('NINJA_SESSION');
		let level = /level=(.*?)(&|$)/.exec(ninjaSession);
		if (level) {
			return level[1];
		} else {
			return '';
		}
	}

	public extractCategory(): string {
		let ninjaSession = Cookies.get('NINJA_SESSION');
		let category = /category=(.*?)(&|$)/.exec(ninjaSession);
		if (category) {
			return category[1];
		} else {
			return '';
		}
	}

	public getUserCookieAgreement(): boolean {
		let ninjaSession = Cookies.get('WB_SESSION');
		let cookie = /agreeCookie=(.*?)(&|$)/.exec(ninjaSession);
		if (cookie) {
			return <boolean><any>cookie[1];
		} else {
			return false;
		}
	}

    saveUserCookieAgreement(isAgreed: boolean) {
        let options = {
            url: '/api/cookie/agree',
            method: RequestMethod.Put,
            search: `agreeCookie=${isAgreed}`
        }

        return httpRequest(this.http, options)
    }

	public loadSettings(): any {
		let request = {
			method: RequestMethod.Get,
			url: '/settings/get/settings.json'
		};

		return httpRequest(this.http, request);
	}

	public setSettings(languageId: string, level: string, category: string): any {
		let params = `languageId=${languageId}&level=${level}&category=${category}`;
		let request = {
			method: RequestMethod.Get,
			url: '/settings/set.json',
			search: params
		};

		return httpRequest(this.http, request);
	}

	public loadLanguages(): any {
		let request = {
			method: RequestMethod.Get,
			url: '/settings/get/languages.json'
		};

		return httpRequest(this.http, request);
	}
}
