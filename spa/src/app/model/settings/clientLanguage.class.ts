export class ClientLanguage {
    public languageId: 					string;
    public countryCode: 				string;
    public languageCode: 				string;
    public shortLanguageCode: 			string;
    public isWebSpeechAPISupported: 	boolean;
    public isNeedSpaces: 				boolean;
}

export const RTL_LANGUAGES = [ 'Arabic', 'Hebrew'];