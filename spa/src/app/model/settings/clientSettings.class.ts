import {ClientLanguage} from './clientLanguage.class';

export class ClientSettings {
    public currentLanguage:		ClientLanguage = new ClientLanguage();
    public currentLevel: 		string;
    public currentCategory: 	string;
}
