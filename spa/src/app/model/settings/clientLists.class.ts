import {ClientLanguage} from './clientLanguage.class';

export class ClientLists {
	public languages: 				ClientLanguage[] = [];
	public levels: 					string[] = [];
	public categories: 				string[] = [];
}
