import { Injectable, NgModule } from '@angular/core';

@Injectable()
export class AudioService {

  private audio: HTMLAudioElement;

  constructor() {
    this.audio = <HTMLAudioElement>document.getElementById('audio');
    if(!this.audio){
      this.audio = document.createElement('audio');
    }
    this.audio.setAttribute('initialized', 'true');
  }

  private isPlaying():boolean {
    return !this.audio.paused
  }

  public play(source: string, playbackRate: number = 0.7):void {
    if(this.isPlaying()) {
      this.pause()
    }

    this.audio.src = source;
    this.audio.playbackRate = playbackRate;

    this.audio.play();
  }

  public pause():void {
    this.audio.pause();
  }
}
