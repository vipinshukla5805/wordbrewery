declare const process: any;
declare const isStandaloneMode: boolean;

export class AppSettings {
   public static get API_ENDPOINT(): string {
       return false && isStandaloneMode && process.env.CORS ?  'http://wordbrewery.azurewebsites.net' : '';
   }
   
   public static get WB_VERSION(): string { return process.env.VERSION || '1.0.0' }
 }
