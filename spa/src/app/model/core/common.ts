export function groupByRow(data: any, numberOfColPerRow: number = 4) {
    let newArr = [];

    if (data.length < numberOfColPerRow) {
        newArr.push(data)
    }
    else
        for (let i = 0; i < data.length; i += numberOfColPerRow) {
            newArr.push(data.slice(i, i + numberOfColPerRow));
        }
    return newArr;
}

export function sortByKey(array, key) {
    if (array.length > 0) {
        let sortedArray = [];
        array.forEach((item, idx) => {
            sortedArray[idx] = item.sort((a, b) => {
                let x = a[key]; let y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            })
        })
        return sortedArray;
    }
}

export const SORTING_OPTIONS = [
    {
        text: 'Recent',
        value: '1',
        selected: true,
        'class': 'flag-icon'
    },
    {
        text: 'Name',
        value: '2',
        selected: false,
        'class': 'flag-icon'
    },
    {
        text: 'Language',
        value: '3',
        selected: false,
        'class': 'flag-icon'
    },
    {
        text: 'Size',
        value: '4',
        selected: false,
        'class': 'flag-icon'
    },
    {
        text: 'Progress',
        value: '5',
        selected: false,
        'class': 'flag-icon'
    },
    {
        text: 'Least Learned',
        value: '6',
        selected: false,
        'class': 'flag-icon'
    },
    {
        text: 'Most Learned',
        value: '7',
        selected: false,
        'class': 'flag-icon'
    }
];

export enum EnumSortingOptions {
    'Recent' = 1,
    'Name',
    'Language',
    'Size',
    'Progress',
    'Least_Learned',
    'Most_Learned'
}