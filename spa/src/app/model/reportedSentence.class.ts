export class ReportedSentence {
    public languageId     : string;
    public text           : string;
    public reportedByUser : string;
    public reason         : string;
    public source         : string;
}
