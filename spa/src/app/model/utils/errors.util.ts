import { Injectable, NgModule } from 	'@angular/core';

@Injectable()
export class Errors {

	// show error messages on the element with id="errorMsg"
	public show(err: any): any {
		if (!err) {
			return;
		}
		if (err._body == null) {
			return;
		}
		try {
			var msg = JSON.parse(err._body);
		} catch ( e ) {
			console.error(e);
        	return;
    	}
		var errorMsg = msg.error;
		// console.log(errorMsg);
		var errorElement = document.getElementById('errorMsg');
		errorElement.innerHTML = errorMsg;
		$('#errorMsg').show();
     		setTimeout((() => {
			$('.wb-alert').fadeOut(10000);
		}).bind(this), 300);
	}
}
