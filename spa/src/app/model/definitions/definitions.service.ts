import { Injectable, NgModule } from 		'@angular/core';
import { Http, Headers, RequestMethod } from 	'@angular/http';
import { Popovers } from 			'../popovers/popovers';
import { httpRequest } from '../core/httpRequest'

export interface IDefinition{
	languageId ?: string
	text       ?: string
	lemma      ?: string
	pos        ?: string
}

@Injectable()
export class DefinitionsService {

  public headers: Headers = new Headers();
  public isGettingDefinition: boolean = false;

  constructor(public http: Http, public popovers: Popovers) { }

	public getWordDefinitions(data: IDefinition){
		let params;
		if(data && data.lemma){
			params =  `languageId=${data.languageId}&lemma=${encodeURIComponent(data.lemma)}`;
		}
		else{
			params =  `languageId=${data.languageId}&text=${encodeURIComponent(data.text)}`;
		}

    let req = {
      method: RequestMethod.Get,
      url: '/api/word/definitions',
      search: params
    };
		return httpRequest(this.http, req);
	}

  // target: popover target element (event.target)
  public getAndShowDefinitions(data: IDefinition, target: HTMLElement): any {
    if (this.isGettingDefinition) {
      return;
    }

    this.isGettingDefinition = true;
    this.getWordDefinitions(data)
    .subscribe(res => {
				this.popovers.hide();
				let content = 'Sorry, there are no definitions for this word.';
	      let definitions = res;
	      if (definitions.length > 0) {
						content = '';
		        target.setAttribute('data-original-title', `Definition:<b> ${data.lemma
							? data.lemma : data.text}</b>`);
						definitions.slice(0, 3).forEach((item) => {
							content += '<p>- ' + item + '</p>'
						});
	      }
				target.setAttribute('data-html', 'true');
	      target.setAttribute('data-content', content);
				$(target).popover('show')
	      setTimeout(() => $(target).popover('destroy'), 2000)
    	},
      null,
      () => {
        this.isGettingDefinition = false;
      });
  }
}
