export class WordListEntry {
	public id: 					string;
	public listId: 				string;
	public text: 				string;
	public date: 				string;
	public lemma: 				string;
	public pos: 				string;
	public frequencyPosition: 	number;
	public numberShown: 		number;
	public isLearned: 			boolean;
	public translation: 		string;
	public definitions: 		string[] = [];
	public examples: 			string[] = [];
}
