export class SentenceListEntry {
	public id: 				string;
	public listId: 			string;
	public text: 			string;
	public date: 			string;
	public source: 			string;
	public score: 			number;
	public unlistedWords: 	number;
	public numberShown: 	number;
	public isLearned: 		boolean;
	public translation: 	string;
}
