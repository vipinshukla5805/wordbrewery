import { Injectable, NgModule } from 		'@angular/core';
import { Http, Headers, RequestMethod } from 	'@angular/http';

import { httpRequest } from '../core/httpRequest';

import { ClientSentence } from 	'../clientSentence.class';

import { Word } from 			'../word.class';

@Injectable()
export class WordListService {

  public headers: Headers = new Headers();

  constructor(public http: Http) { }

  

  public getList(id: string): any {
    let params = `id=${encodeURIComponent(id)}`;
    let req = {
      url: '/lists/words/get.json',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  public getlists(): any {
    let req = {
      url: '/lists/words/getlists.json',
      method: RequestMethod.Get
    }

    return httpRequest(this.http, req)
  }

  public getlistsByLanguage(languageId: string): any {
    let params = `languageId=${encodeURIComponent(languageId)}`;
    let req = {
      url: '/lists/words/getlists.json',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  public addlist(listName: string, description: string = '', languageId: string): any {
    let params = `listName=${encodeURIComponent(listName)}&description=${encodeURIComponent(description)}&languageId=${encodeURIComponent(languageId)}`;
    let req = {
      url: '/lists/words/create.json',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  public renameList(id: string, newName: string, description: string): any {
    let params = `id=${encodeURIComponent(id)}&newName=${encodeURIComponent(newName)}`;
    let req = {
      url: '/lists/words/renlist.json',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  public changeListDescription(id: string, description: string): any {
    let params = `id=${encodeURIComponent(id)}&description=${encodeURIComponent(description)}`;
    let req = {
      url: '/lists/words/changedescr.json',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  public deleteList(id: string): any {
    let params = `id=${encodeURIComponent(id)}`;
    let req = {
      url: '/lists/words/dellist.json',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  // *********************** entries *************************************************

  public addEntry(clientSentence: ClientSentence, listId: string): any {
    let params = `listId=${encodeURIComponent(listId)}`;
    let req = {
      url: '/lists/words/addentry.json',
      method: RequestMethod.Post,
      search: params,
      body: JSON.stringify(clientSentence)
    }

    return httpRequest(this.http, req)
  }

  public deleteEntry(listId: string, entryId: string): any {
    let params = `listId=${encodeURIComponent(listId)}&entryId=${encodeURIComponent(entryId)}`;
    let req = {
      url: '/lists/words/delentry.json',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  public markLearned(listId: string, entryId: string, isLearned: boolean): any {
    let params = `listId=${encodeURIComponent(listId)}&entryId=${encodeURIComponent(entryId)}&isLearned=${encodeURIComponent(isLearned.toString())}`;
    let req = {
      url: '/lists/words/marklearned.json',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  public getEntries(listId: string): any {
    let params = `listId=${encodeURIComponent(listId)}`;
    let req = {
      url: '/lists/words/getentries.json',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }

  getSuggestedWord(languageId: string, level: string) { 
    let params = `languageId=${encodeURIComponent(languageId)}&level=${encodeURIComponent(level)}`;
    let req = {
      url: '/api/word/suggested',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req)
  }
}