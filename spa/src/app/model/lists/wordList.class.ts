export class WordList {
  public id: string;
  public userName: string;
  public name: string;
  public languageId: string;
  public dateCreated: string;
  public description: string;
  public total: number;
  public learned: number;
  public excerpt: string;
}
