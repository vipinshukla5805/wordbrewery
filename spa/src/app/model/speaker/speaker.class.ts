import
{
	Injectable, Inject, NgModule
} from '@angular/core';
import
{
	TranslatorService
} from '../translator/translator.service';
import { AudioService } from '../core/audio'

const SPEAK_URL ='/api/tts/speak';
const ACCESS_TOKEN = 'wb_access-token';
const USER_LEVEL_RATE = {
	Beginner     : 0.75,
	Intermediate : 0.85
};

@Injectable()
export class SpeakerService {
		public isWebSpeechAPISupportedByBrowser: boolean;
    public langVoices: Array<SpeechSynthesisVoice>;

		constructor(private translatorSvc: TranslatorService, private audio: AudioService){
	    	this.initWebSpeechAPI();
		}

    public initAudio(rate: number = 0.7): any {
    	// this.initializeAudio(null);
    }

		public isLanguageSpeakSupported(lang : string): boolean{
			 return $.inArray(lang, this.translatorSvc.supportedLanguages) > -1;
		}

    public initWebSpeechAPI(): any {
    	// Need to call in advance so it worked in Chrome
    	if ((<any>window).speechSynthesis == null) {
			console.log('window.speechSynthesis is null');
       		return;
       	}
       	// var synth = (<any>window).speechSynthesis;
       	// var voices = synth.getVoices();
    }

    public speak(text: string, languageId: string, level: string): any {
				// if(false && !this.isLanguageSpeakSupported(languageId)){
				// 		return false;
				// }

				let rate = USER_LEVEL_RATE[level] || 1 ;
				this.audio.play(`${SPEAK_URL}?languageId=${languageId}&text=${text}`, rate);
  	}

  public isWebSpeechAPISupported(shortLanguageCode: string): any {
			if ((<any>window).speechSynthesis == null) {
	       		return false;
	    }

      let voices = (<any>window).speechSynthesis.getVoices();
			this.langVoices = new Array();
			this.langVoices = voices.filter((voice) => voice.lan.startsWith(shortLanguageCode));
			return this.langVoices != null && this.langVoices.length > 0;
	}
}
