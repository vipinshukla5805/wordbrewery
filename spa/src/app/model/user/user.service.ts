import { Injectable, NgModule } from 		'@angular/core';
import { Http, Headers, RequestMethod } from 	'@angular/http';
import { httpRequest } from '../core/httpRequest'
import { IUserData } from './user.class'

export interface IUserResponse {
  status: boolean
  message?: string
  data: IUserData | any
}

@Injectable()
export class UserService {
  constructor(private http: Http) { }

  doGoogleLogin(token: string) {
    let req = {
      url: '/api/user/login/google',
      method: RequestMethod.Post,
      search: `idtoken=${token}`
    }

    return httpRequest(this.http, req);
  }

  doFacebookLogin(token: string) {
    let req = {
      url: '/api/user/login/facebook',
      method: RequestMethod.Post,
      search: `accessToken=${token}`
    }

    return httpRequest(this.http, req);
  }

  login(email: string, password: string) {
    let params = `userEmail=${email}&userPassword=${password}&rememberMe=true`

    let req = {
      url: '/api/user/login',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req);
  }

  register(email: string, password: string) {
    let params = `userEmail=${email}&userPassword=${password}`

    let req = {
      url: '/api/user/register',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req);
  }

  activate(email: string, code: string) {
    let params = `userEmail=${email}&code=${code}`

    let req = {
      url: '/api/user/activate',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req);
  }

  resend(email: string) {
    let params = `userEmail=${email}`

    let req = {
      url: '/api/user/resend',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req);
  }

  forgot(email: string) {
    let params = `userEmail=${email}`

    let req = {
      url: '/api/user/forgot',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req);
  }

  reset(password: string) {
    let params = `password=${password}`

    let req = {
      url: '/api/user/reset',
      method: RequestMethod.Get,
      search: params
    }

    return httpRequest(this.http, req);
  }

  logout() {
    let req = {
      url: '/api/user/logout',
      method: RequestMethod.Get
    }

    return httpRequest(this.http, req);
  }

  getUser(): any {
    let req = {
      url: '/api/user',
      method: RequestMethod.Get
    }

    return httpRequest(this.http, req);
  }

  getUserStatistics(level: string, languageId: string = 'ALL', params: string = 'SENTENCES_SHOWN'): any {
    let parms = `param=${params}&level=${level}&languageId=${languageId}`;
    let req = {
      url: '/api/user/stats',
      method: RequestMethod.Get,
      search: parms
    }

    return httpRequest(this.http, req);
  }

  getHops(): any {
    let parms = `param=HOPS`;
    let req = {
      url: '/api/user/stats',
      method: RequestMethod.Get,
      search: parms
    }

    return httpRequest(this.http, req);
  }

  getProfileUser(): any {
    let req = {
      url: '/api/user/profile',
      method: RequestMethod.Get
    }

    return httpRequest(this.http, req);
  }

  doesUserExist(userEmail: string): any {
    let req = {
      url: '/api/user/check',
      method: RequestMethod.Get,
      search: `userEmail=${userEmail}`
    }

    return httpRequest(this.http, req);
  }

  uploadAvatar(avatarEncodedImage: string): any {
    let req = {
      url: '/api/user/avatar',
      method: RequestMethod.Put,
      body: JSON.stringify(avatarEncodedImage)
    }

    return httpRequest(this.http, req);
  }

  uploadBio(text: string): any {
    let req = {
      url: '/api/user/bio',
      method: RequestMethod.Put,
      body: JSON.stringify(text)
    }

    return httpRequest(this.http, req);
  }
}