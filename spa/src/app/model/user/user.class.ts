export class User {
  public email: string;
  public role: string;
  public isActivated: boolean;
}

export interface IUserData {
	 email: string;
	 role: string;
	 isActivated: boolean;
}

export interface IProfileData {
  userEmail: string;
  userName: string;
  location: string;
  nativeLanguage: string;
  dayOfBirth: number;
  monthOfBirth: number;
  yearOfBirth: number;
  avatarUrlSmall: string;
  avatarUrlMedium: string;
}

export interface IUserStatisticData {
  status: boolean;
  message: string;
  data: any;
}
