import { Injectable, NgModule, Injector } from '@angular/core';
import { Router  } from '@angular/router';

import { Observable } from 'rxjs/Observable'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

import { UserService, IUserResponse, } from './user.service'
import { IUserData, IUserStatisticData, IProfileData } from './user.class'
export { IUserResponse, IUserData, IUserStatisticData, UserService, IProfileData }

@Injectable()
export class AuthenticationService {
  isLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  userData$: BehaviorSubject<IUserData> = new BehaviorSubject<IUserData>(null);
  userProfileData$: BehaviorSubject<IProfileData> = new BehaviorSubject<IProfileData>(null);

  constructor(private _user: UserService) { }

  getUser() {
    let hasValue = !!sessionStorage.getItem('user');
    if (!hasValue) {
      this._user.getUser()
        .subscribe((res: IUserResponse) => {
          this.isLoggedIn$.next(res.status);
          if (res.status) {
            sessionStorage.setItem('user', JSON.stringify(res.data))
            this.userData$.next(res.data);
          }
        });
    }
    else {
      this.isLoggedIn$.next(true)
      this.userData$.next(JSON.parse(sessionStorage.getItem('user')));
    }

    this.getProfileUser();
  }

  getProfileUser(refresh: boolean = false) {
    let hasValue = !!sessionStorage.getItem('user_profile');
    if (refresh || !hasValue) {
      this._user.getProfileUser()
        .subscribe((res: IUserResponse) => {
          if (res.status) {
            sessionStorage.setItem('user_profile', JSON.stringify(res.data))
            this.userProfileData$.next(res.data);
          }
        });
    }
    else {
      this.userProfileData$.next(JSON.parse(sessionStorage.getItem('user_profile')));
    }
  }

  getUserStatistics(level: string, languageId?: string, params?: string) {
    return this._user.getUserStatistics(level, languageId, params);
  }

  getHops() {
    return this._user.getHops()
  }

  doesUserExist(userEmail: string) {
    return this._user.doesUserExist(userEmail);
  }

  logout() {
    sessionStorage.removeItem("user");
    return this._user.logout();
  }

  login(email: string, password: string) {
    return this._user.login(email, password);
  }

  register(email: string, password: string) {
    return this._user.register(email, password);
  }

  forgotPassword(email: string) {
    return this._user.forgot(email);
  }

  resetPassword(password: string) {
    return this._user.reset(password);
  }

  resendActivation(email: string) {
    return this._user.resend(email);
  }

  doGoogleLogin(token: string) {
    return this._user.doGoogleLogin(token);
  }

  doFacebookLogin(token: string) {
    return this._user.doFacebookLogin(token);
  }
}
