import {Injectable, NgModule} from 		'@angular/core';
import {Http, Headers} from 	'@angular/http';

@Injectable()
export class SearchService {

  public headers: Headers = new Headers();

  constructor(public http: Http) { }

  public search(term: string, languageId: string, sentencesToFilter: string[]): any {
    console.log('search');
    this.headers.set('Accept', 'application/json');
    this.headers.set('Content-Type', 'application/json');

    var endpoint = '/sentence/search.json';
    var params = `?term=${encodeURIComponent(term) }&languageId=${encodeURIComponent(languageId) }`
      + `&filter=${encodeURIComponent(sentencesToFilter.join('|')) }`;

    return this.http.get(endpoint + params, { headers: this.headers })
      .map(res => res.json());
  }
}
