const gulp = require("gulp");
const del = require("del");
const tsc = require("gulp-typescript");
const sourcemaps = require('gulp-sourcemaps');
const tsProject = tsc.createProject("tsconfig.json", {
  typescript: require('typescript')
});
const tslint = require('gulp-tslint');
const sass = require('gulp-sass');
const csso = require('gulp-csso');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const minifyCSS = require('gulp-minify-css');
const webpack = require('gulp-webpack');
const uglify = require('gulp-uglify');
const rimraf = require('gulp-rimraf');

var bs = require('browser-sync').create();


/**
 * Remove build directory.
 */
gulp.task('clean', function (cb) {
  return del([ "build" ], cb);
});

gulp.task('js-clean', function () {
  return gulp.src([ './src/**/*.js', './src/**/*.js.map' ], { read: false })
    .pipe(rimraf({ force: true }));
});

gulp.task('server', function (cb) {
  bs.init({
    port: 3000,
    server: {
      baseDir: "./build"
    }
  });
  gulp.watch([
    "build/**/*.js",
    "build/**/**/*.js",
    "build/**/**/**/*.js",
    "build/**/*.css",
    "build/**/**/*.css",
    "build/**/*.html",
    "build/**/**/*.html",
    "build/**/**/**/*.html" ], bs.reload, cb)
});

/**
 * Compile TypeScript sources and create sourcemaps in build directory.
 */
gulp.task('webpack', function () {
  return gulp.src([ 'src/app/main.ts', 'src/app/vendor.ts' ])
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('build/dist/'));
});

/**
 * Compile scss sources and create sourcemaps in build directory.
 */
gulp.task('styles', function () {
  gulp.src("src/styles/style.scss")
    .pipe(sass({
      outputStyle: 'nested',
      sourceComments: true,
      errLogToConsole: true
    }))
    .pipe(csso())
    .pipe(minifyCSS())
    .pipe(rename('index.min.css'))
    .pipe(gulp.dest('build/css'));
});

gulp.task('fonts', function () {
  gulp.src([ "src/fonts/**" ])
    .pipe(gulp.dest('build/fonts'));
});

gulp.task('images', [ 'css-images' ], function () {
  gulp.src([ "src/images/**", "src/images/**/**" ])
    .pipe(gulp.dest('build/images'));
});

gulp.task('css-images', function () {
  gulp.src([ "src/styles/lib/flipcountdown/img/**" ])
    .pipe(gulp.dest('build/css/img'));
});

/**
 * Copy all css that are not scss files into build directory.
 */
gulp.task("css", function () {
  return gulp.src([ "src/app/*.css", "src/styles/*.css", "!src/**/*.scss" ])
    .pipe(minifyCSS())
    .pipe(gulp.dest('build/css'));
});

/**
 * Copy all resources that are not TypeScript files into build directory.
 */
gulp.task("resources", function () {
  return gulp.src([ "src/*.html", "src/**/*.html" ])
    .pipe(gulp.dest("build"))
});

/**
 * Copy all required libraries into build directory.
 */
gulp.task("libs", function () {
  return gulp.src([
    'font-awesome/css/**',
    'font-awesome/fonts/**',
    'bootstrap/dist/**',
    'jquery/dist/**'
  ], { cwd: "node_modules/**" }) /* Glob required here. */
    .pipe(gulp.dest("build/lib"));
});

gulp.task('reload', function () {
  bs.reload();
})

gulp.task('watch', function () {
  gulp.watch([ "src/**/*.ts", "src/**/**/*.ts", "src/**/**/**/*.ts" ], [ 'webpack', 'js-clean' ]);
  gulp.watch([ "src/**/*.scss", "src/**/**/*.scss" ], [ 'styles', 'css' ]);
  gulp.watch([ "src/**/*.html", "src/**/**/*.html", "src/**/**/**/*.html" ], [ 'resources' ]);
});

//Copy all to java folder
gulp.task("serve", function () {
  return gulp.src("build/")
    .pipe(gulp.dest("../../target/classes/spa/"))
});

/**
 * Build the project.
 */
gulp.task("build", [ 'webpack', 'styles', 'css', 'libs', 'fonts', 'images', "resources", 'js-clean' ], function () {
  console.log("Building the project ...")
});
