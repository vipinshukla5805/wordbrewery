## Prerequisites

* Node.js 4.2.2 or later
* npm install gulp -g

## Setup

After cloning the repository (and when a branch is checked out):

* `npm install`

* `npm run clean && npm run build`

* `npm start`
